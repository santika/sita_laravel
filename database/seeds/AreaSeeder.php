<?php

use Illuminate\Database\Seeder;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Area::create([
            'area' => 'Ubud',
            'lat' => '-8.504109',
            'lng' => '115.262530'
        ]);
        App\Models\Area::create([
            'area' => 'Seminyak',
            'lat' => '-8.692900',
            'lng' => '115.158669'
        ]);
        App\Models\Area::create([
            'area' => 'Kuta',
            'lat' => '-8.720314',
            'lng' => '115.169245'
        ]);
        App\Models\Area::create([
            'area' => 'Sanur',
            'lat' => '-8.702491',
            'lng' => '115.264907'
        ]);
        App\Models\Area::create([
            'area' => 'Canggu',
            'lat' => '-8.659742',
            'lng' => '115.130480'
        ]);
        App\Models\Area::create([
            'area' => 'Legian',
            'lat' => '-8.703898',
            'lng' => '115.164707'
        ]);
        App\Models\Area::create([
            'area' => 'Jimbaran',
            'lat' => '-8.769597',
            'lng' => '115.168991'
        ]);
        App\Models\Area::create([
            'area' => 'Nusa Dua',
            'lat' => '-8.827410',
            'lng' => '115.220186'
        ]);
    }
}
