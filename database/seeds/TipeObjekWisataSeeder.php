<?php

use Illuminate\Database\Seeder;

class TipeObjekWisataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\TipeObjekWisata::create([
            'nama_tipe' => 'Outdoor'
        ]);
        App\Models\TipeObjekWisata::create([
            'nama_tipe' => 'Pantai'
        ]);
        App\Models\TipeObjekWisata::create([
            'nama_tipe' => 'Taman'
        ]);
        App\Models\TipeObjekWisata::create([
            'nama_tipe' => 'Perkebunan'
        ]);
        App\Models\TipeObjekWisata::create([
            'nama_tipe' => 'Museum'
        ]);
        App\Models\TipeObjekWisata::create([
            'nama_tipe' => 'Kursus'
        ]);
        App\Models\TipeObjekWisata::create([
            'nama_tipe' => 'Galeri Seni'
        ]);
        App\Models\TipeObjekWisata::create([
            'nama_tipe' => 'Olahraga'
        ]);
        App\Models\TipeObjekWisata::create([
            'nama_tipe' => 'Indoor'
        ]);
    }
}
