<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\User::create([
            'email' => 'admin@admin.com',
            'password' => \Hash::make('4d3k1234'),
            'role' => 'admin'
        ]);

        App\Models\User::create([
            'email' => 'santika.ifreak@gmail.com',
            'password' => Hash::make('4d3k1234'),
            'nama_depan' => 'santika',
            'nama_belakang' => 'putra',
            'jk' => 'laki-laki',
            'tgl_lahir' => '1995-11-12',
            'alamat' => 'denpasar',
            'no_telpon' => '081529321843',
        ]);
    }
}