<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_hotels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_transaksi');
            $table->string('order_name', 50);
            $table->string('order_name_detail', 30);
            $table->string('link_gambar');
            $table->string('lat', 15);
            $table->string('lng', 15);
            $table->decimal('harga', 10, 2);
            $table->decimal('harga_plus_pajak', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_hotels');
    }
}
