<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobils', function (Blueprint $table) {
            $table->increments('id');
            $table->string('merk', 20);
            $table->string('model', 50);
            $table->enum('jenis', ['Hatchback','Minivan','SUV','Sedan','Minibus']);
            $table->integer('jumlah_penumpang');
            $table->integer('harga_supir_bensin');
            $table->integer('harga_kosongan');
            $table->string('image_path', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobils');
    }
}
