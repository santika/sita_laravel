<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->integer('id_area');
            $table->datetime('tgl_transaksi');
            $table->string('asal', 4);
            $table->integer('dana');
            $table->date('start');
            $table->date('end');
            $table->integer('dewasa', 2);
            $table->integer('anak', 2);
            $table->integer('batita', 2);
            $table->integer('order_id_pesawat');
            $table->integer('order_id_hotel');
            $table->string('token_pesawat', 45);
            $table->string('token_hotel', 45);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
