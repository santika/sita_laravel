<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjekWisatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objek_wisatas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_area');
            $table->string('nama', 50);
            $table->text('deskripsi');
            $table->string('alamat', 50);
            $table->integer('harga_dewasa');
            $table->integer('harga_anak');
            $table->string('lat', 15);
            $table->string('lng', 15);
            $table->enum('hari_buka', ['Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu']);
            $table->time('jam_buka');
            $table->enum('hari_tutup', ['Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu']);
            $table->time('jam_tutup');
            $table->integer('durasi', 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objek_wisatas');
    }
}
