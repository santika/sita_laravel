<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiWisatawansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_wisatawans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_transaksi');
            $table->string('title', 5);
            $table->string('nama_depan', 50);
            $table->string('nama_belakang', 50);
            $table->date('tgl_lahir');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_wisatawans');
    }
}
