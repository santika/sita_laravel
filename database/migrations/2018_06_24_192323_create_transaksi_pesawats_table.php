<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiPesawatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_pesawats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_transaksi');
            $table->string('order_name');
            $table->string('order_name_detail');
            $table->string('logo');
            $table->decimal('harga', 10, 2);
            $table->decimal('harga_plus_pajak', 10, 2);
            $table->date('tgl_berangkat');
            $table->time('jam_berangkat');
            $table->time('jam_sampai');
            $table->enum('pulang_pergi', ['Pulang', 'Pergi']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_pesawats');
    }
}
