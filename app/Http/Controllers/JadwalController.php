<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TransaksiObjekWisata;
use App\Models\TransaksiHotel;
use App\Models\TransaksiPesawat;
use App\Models\Transaksi;
use App\Models\Jadwal;
use App\Http\Controllers\Api\Map;

class JadwalController extends Controller
{
    private $hotel;
    private $objekwisata;
    private $pesawat;
    private $key;
    private $monitor;

    function index(Request $request, $id)
    {
        $jadwals = Jadwal::with(['transaksi.hotel', 'transaksi.pesawat'])
            ->where('id', $id)
            ->get();

        return view('jadwal', compact('jadwals', 'id'));
    }

    function download($id)
    {
        $jadwals = Jadwal::with(['transaksi.hotel', 'transaksi.pesawat'])
            ->where('id', $id)
            ->get();

        $pdf = \PDF::loadView('pdf', compact('jadwals'));

        foreach ($jadwals as $jadwal) {
            $transaksi = Transaksi::find($jadwal->id_transaksi);
            return $pdf->download('Tour_'.$transaksi->start.'_-_'.$transaksi->end.'.pdf');
        }
    }

    function generate(Request $request)
    {
        $this->key = 'AIzaSyChrdDRWwH5tZRc_wDGUGT8gjcTrEIJEns';
        $id = $request->input('id');
 
        $this->objekwisata = TransaksiObjekWisata::with(['objekWisata'])
            ->where('id_transaksi', $id)
            ->get()
            ->toArray();
        $this->hotel = TransaksiHotel::where('id_transaksi', $id)
            ->get()
            ->toArray();
        $this->pesawat = TransaksiPesawat::where('id_transaksi', $id)
            ->get()
            ->toArray();
        $data = $this->scheduling();

        $jadwal = Jadwal::create([
            'id_transaksi' => $id,
            'jadwal' => json_encode($data)]);

        return response()->json($jadwal->id, 200);
    }

    public function monitor($id)
    {
        header("Content-Type: text/html");
        header("Cache-Control: no-cache");
        header("Connection: keep-alive");

        $this->chunk('First Init');
        $this->monitor = true;
        $this->key = 'AIzaSyChrdDRWwH5tZRc_wDGUGT8gjcTrEIJEns';

        $this->objekwisata = TransaksiObjekWisata::with(['objekWisata'])
            ->where('id_transaksi', $id)
            ->get()
            ->toArray();
        $this->hotel = TransaksiHotel::where('id_transaksi', $id)
            ->get()
            ->toArray();
        $this->pesawat = TransaksiPesawat::where('id_transaksi', $id)
            ->get()
            ->toArray();

        $data = $this->scheduling();
    }

    private function chunk($text, $selected = null)
    {
        if ($this->monitor) {
            echo "
                <table border=1>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Nama Destinasi</th>
                            <th>Jarak</th>
                            <th>Waktu Tempuh</th>
                        </tr>
                    </thead>
                    <tbody>
            ";
            for ($i = 0; $i < count($text); $i++) {
                $selectedStyle = '';
                if ($selected != null && $i == $selected) {
                    $selectedStyle = 'style="color:#fff; background-color:#000;"';
                }
                echo '
                    <tr '.$selectedStyle.'>
                        <td>'.($i+1).'</td>
                        <td>'.$text[$i]['routes'][0]['legs'][0]['end_address'].'</td>
                        <td>'.$text[$i]['routes'][0]['legs'][0]['distance']['text'].'</td>
                        <td>'.$text[$i]['routes'][0]['legs'][0]['duration_in_traffic']['text'].'</td>
                    </tr>
                ';
            }
            echo "
                    </tbody>
                </table>
                <hr>
            ";
            if (!is_array($text)) {
                echo $text;
                echo '<br>';
            }
        }
        ob_flush();
        flush();
    }

    private function scheduling()
    {
        $objekWisata = $this->objekwisata;
        $o = count($objekWisata)+2;
        $i = 0;
        $sebelumnyaHotel = false;

        while ($i <= $o) {
            if ($i == $o) {
                $nearest = $this->toHotel($this->pesawat[1]['tgl_berangkat'].$this->pesawat[1]['jam_berangkat'],false,true,false,true);
            } elseif ($i == 0) {
                $nearest = $this->toHotel($this->pesawat[0]['tgl_berangkat'].$this->pesawat[0]['jam_sampai'],true);
            } else if ($i > 0 && $i < ($o-1)) {
                if ($i == 1) {
                    $nearest = $this->getNearest($this->hotel,$objekWisata,$schedule[0]['time']['stayMinimumUntil'],$schedule[0]['time']['nextStart'],true);
                    unset($objekWisata[$nearest['index']]);
                    $objekWisata=array_values($objekWisata);
                } else {
                    if (count($schedule) > 1 && $schedule[$i-1]['time']['continue'] === false) {
                        if ($sebelumnyaHotel) {
                            $latlng = $this->hotel[0]['lat'].','.$this->hotel[0]['lng'];
                        } else {
                            $latlng = $schedule[$i-1]['objekwisata']['objek_wisata']['lat'].','.$schedule[$i-1]['objekwisata']['objek_wisata']['lng'];
                        }

                        $sebelumnyaHotel = true;
                        $nearest = $this->toHotel($nearest['time']['stayMinimumUntil'],false,true,$latlng);
                        $o++;
                    } else {
                        if ($sebelumnyaHotel) {
                            $latlng = $this->hotel;
                        } else {
                            $latlng = $schedule[$i-1]['objekwisata'];
                        }

                        $nearest = $this->getNearest($latlng,$objekWisata,$nearest['time']['stayMinimumUntil'],$nearest['time']['nextStart'],$sebelumnyaHotel);
                        unset($objekWisata[$nearest['index']]);
                        $objekWisata = array_values($objekWisata);
                        $sebelumnyaHotel = false;
                    }
                }
            } elseif ($i == ($o-1)) {
                $nearest = $this->toHotel($nearest['time']['stayMinimumUntil'],false,true,$schedule[$i-1]['objekwisata']['objek_wisata']['lat'].','.$schedule[$i-1]['objekwisata']['objek_wisata']['lng']);
            }

            $schedule[$i] = $nearest;
            $i++;
        }
        return array('schedule' => $schedule);
    }

    private function getNearest($a,$b,$time,$continueAtSameDay,$hotel=false)
    {
        for ($i = 0; $i < count($b); $i++) {
            if ($hotel) {
                $payload = [
                    'origin' => $a[0]['lat'].','.$a[0]['lng'],
                    'destination' => $b[$i]['objek_wisata']['lat'].','.$b[$i]['objek_wisata']['lng'],
                    'departure_time' => strtotime($time),
                    'traffic_model' => 'pessimistic',
                    'language' => 'id',
                    'key' => $this->key
                ];
            } else {
                $payload = [
                    'origin' => $a['objek_wisata']['lat'].','.$a['objek_wisata']['lng'],
                    'destination' => $b[$i]['objek_wisata']['lat'].','.$b[$i]['objek_wisata']['lng'],
                    'departure_time' => strtotime($time),
                    'traffic_model' => 'pessimistic',
                    'language' => 'id',
                    'key' => $this->key
                ];
            }

            $execute[$i] = \Curl::to('https://maps.googleapis.com/maps/api/directions/json')
                ->withData($payload)
                ->get();
            $execute[$i] = json_decode($execute[$i], true);
            // $execute[$i]['latlong']=
            $distance[$i] = $execute[$i]['routes'][0]['legs'][0]['duration_in_traffic']['value'];
        }

        $nearest = array_keys($distance, min($distance));
        $this->chunk($execute, $nearest[0]);

        $distanceDuration['distance']['text'] = $execute[$nearest[0]]['routes'][0]['legs'][0]['distance']['text'];
        $distanceDuration['distance']['value'] = $execute[$nearest[0]]['routes'][0]['legs'][0]['distance']['value'];
        $distanceDuration['duration']['text'] = $execute[$nearest[0]]['routes'][0]['legs'][0]['duration_in_traffic']['text'];
        $distanceDuration['duration']['value'] = $execute[$nearest[0]]['routes'][0]['legs'][0]['duration_in_traffic']['value'];

        if ($continueAtSameDay === true) {
            $startTime=$time;
        } else {
            $startTime = $continueAtSameDay;
        }

        $hour = $this->timeCounter($startTime,$execute[$nearest[0]]['routes'][0]['legs'][0]['duration_in_traffic']['value'],$b[$nearest[0]]['objek_wisata']['durasi'].' Minute');
        return [
            'tipe' => 'objekwisata',
            'objekwisata' => $b[$nearest[0]],
            'distanceDuration' => $distanceDuration,
            'time' => $hour,
            'index' => $nearest[0]
        ];
    }

    private function toHotel($startTime,$first,$reverse=false,$latlng=false,$islast=false)
    {
        if ($latlng === false) {
            $latlng='-8.7467172,115.166787';
        }

        if ($islast) {
            $payload = [
                'origin' => $this->hotel[0]['lat'].','.$this->hotel[0]['lng'],
                'destination' => $latlng,
                'arrival_time' => strtotime($this->pesawat[1]['tgl_berangkat'].$this->pesawat[1]['jam_berangkat']),
                'language' => 'id',
                'key' => $this->key
            ];
        } elseif ($reverse) {
            $payload = [
                'origin' => $this->hotel[0]['lat'].','.$this->hotel[0]['lng'],
                'destination' => $latlng,
                'departure_time' => strtotime($this->pesawat[0]['tgl_berangkat'].$this->pesawat[0]['jam_sampai']),
                'traffic_model' => 'pessimistic',
                'language' => 'id',
                'key' => $this->key
            ];
        } else {
            $payload = [
                'origin' => $latlng,
                'destination' => $this->hotel[0]['lat'].','.$this->hotel[0]['lng'],
                'departure_time' => strtotime($this->pesawat[0]['tgl_berangkat'].$this->pesawat[0]['jam_sampai']),
                'traffic_model' => 'pessimistic',
                'language' => 'id',
                'key' => $this->key
            ];
        }

        $execute = \Curl::to('https://maps.googleapis.com/maps/api/directions/json')
            ->withData($payload)
            ->get();
        $execute = json_decode($execute, true);

        if ($islast) {
            $distanceDuration['distance']['text'] = $execute['routes'][0]['legs'][0]['distance']['text'];
            $distanceDuration['distance']['value'] = $execute['routes'][0]['legs'][0]['distance']['value'];
            $distanceDuration['duration']['text'] = $execute['routes'][0]['legs'][0]['duration']['text'];
            $distanceDuration['duration']['value'] = $execute['routes'][0]['legs'][0]['duration']['value'];
        } else {
            $distanceDuration['distance']['text'] = $execute['routes'][0]['legs'][0]['distance']['text'];
            $distanceDuration['distance']['value'] = $execute['routes'][0]['legs'][0]['distance']['value'];
            $distanceDuration['duration']['text'] = $execute['routes'][0]['legs'][0]['duration_in_traffic']['text'];
            $distanceDuration['duration']['value'] = $execute['routes'][0]['legs'][0]['duration_in_traffic']['value'];
        }

        if ($islast) {
            $startFlight = date('d-m-Y H:i',strtotime($startTime)-(120*60));
            $startDriving = date('d-m-Y H:i',strtotime($startFlight)-$distanceDuration['duration']['value']);
            $arrival = [
                'go' => $startDriving,
                'arrive' => $startFlight,
                'stayMinimumUntil' => null,
                'continue' => false,
                'nextStart' => null
            ];
            $tipe='last';
        } elseif ($first) {
            $arrival=$this->timeCounter($startTime,$distanceDuration['duration']['value'],'2 hour');
            $tipe='hotel';
        } elseif(!$first) {
            $arrival=$this->timeCounter($startTime,$distanceDuration['duration']['value'],'2 hour',false);
            $tipe='hotel';
        }

        return [
            'tipe' => $tipe,
            $distanceDuration,
            'time' => $arrival];
    }

    private function timeCounter($start,$plus,$minimumDurationStay,$firstHotel=true)
    {
        $start = date('d-m-Y H:i',strtotime($start));
        $date = date('d-m-Y',strtotime($start));
        $arrive = date('d-m-Y H:i',(strtotime($start) + $plus));
        $stayUntil = date('d-m-Y H:i',strtotime($arrive) + (strtotime('+ '.$minimumDurationStay) - strtotime('now')));

        if ($firstHotel) {
            $continue=(strtotime($stayUntil) < strtotime($date.' 5 pm')) ? true : false;
            $nextStart = ($continue) ? $stayUntil : date('d-m-Y',strtotime($date)+(strtotime('+1 day')-strtotime('now'))).' 09:00:00' ;
        } else {
            $continue=true;
            $nextStart = (strtotime($stayUntil) < strtotime($date.' 5 pm')) ? $stayUntil : date('d-m-Y',strtotime($date)+(strtotime('+1 day')-strtotime('now'))).' 09:00:00' ;
        }

        return [
            'go' => $start,
            'arrive' => $arrive,
            'stayMinimumUntil' => $stayUntil,
            'continue' => $continue,
            'nextStart' => $nextStart
        ];
    }
}
