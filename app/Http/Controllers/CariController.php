<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Area;
use App\Models\Mobil;
use App\Models\ObjekWisata;
use App\Models\Transaksi;
use App\Models\TransaksiHotel;
use App\Models\TransaksiMobil;
use App\Models\TransaksiObjekWisata;
use App\Models\TransaksiPesawat;
use App\Models\TransaksiWisatawan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CariController extends Controller
{
    public function pesawat(Request $request)
    {
        $start = Carbon::parse($request->session()->get('parameter.start'));
        $end = Carbon::parse($request->session()->get('parameter.end'));

        return view('cari.pesawat', compact('start', 'end'));
    }

    public function pesawatAwal(Request $request)
    {
        $flight = Api\Tiket::pesawatAwal($request);

        if (
            !is_null($flight)
            && array_key_exists('departures', $flight)
            && array_key_exists('result', $flight['departures']) 
            && isset($flight['departures']['result'])
        ) {
            $result = $flight['departures']['result'];
            $prices = array_column($result, 'price_value');
            $min = $result[array_search(min($prices), $prices)];
            
            $termurah='
                <div class="item" id="'. $min['flight_id'] .'">
                    <div class="ui tiny image">
                        <img src="'. $min['image'].'" alt="'. $min['airlines_name'].' Logo">
                    </div>
                    <div class="content">
                        <div class="header" data-tooltip="'. $min['duration'].'" data-inverted>
                            '. $min['simple_departure_time'].' - '. $min['simple_arrival_time'].'
                        </div>
                        <div class="meta">
                            <span class="departure">'. $min['departure_city_name'].' ('. $min['departure_city'].')</span>
                            <span>-</span>
                            <span class="arrival">'. $min['arrival_city_name'].' ('. $min['arrival_city'].')</span>
                            <span class="flight_number">'. $min['flight_number'].'</span>
                            <span class="price selected pergi">Rp ' . number_format($min['price_value'],0,'.',',').'</span>
                        </div>
                    </div>
                </div>
            ';
            for ($i=0; $i<count($result); $i++):
                $item[$i]='
                    <div class="item" id="'. $result[$i]['flight_id'] .'">
                        <div class="ui tiny image">
                            <img src="'. $result[$i]['image'].'" alt="'. $result[$i]['airlines_name'].' Logo">
                        </div>
                        <div class="content">
                            <div class="header" data-tooltip="'. $result[$i]['duration'].'" data-inverted>
                                '. $result[$i]['simple_departure_time'].' - '. $result[$i]['simple_arrival_time'].'
                            </div>
                            <div class="meta">
                                <span class="departure">'. $result[$i]['departure_city_name'].' ('. $result[$i]['departure_city'].')</span>
                                <span>-</span>
                                <span class="arrival">'. $result[$i]['arrival_city_name'].' ('. $result[$i]['arrival_city'].')</span>
                                <span class="flight_number">'. $result[$i]['flight_number'].'</span>
                                <span class="price">Rp ' . number_format($result[$i]['price_value'],0,'.',',').'</span>
                            </div>
                            <div class="extra">
                                <div class="ui right floated primary button" onclick="pilihPesawat(\'pergi\',\''. $result[$i]['flight_id'] .'\')">
                                    Pilih
                                    <i class="right plane icon"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                ';
            endfor;
            $item=implode($item);

            return response()->json(['min' => $termurah, 'full' => $item], 200);
        }

        return response('Gagal Mendapatkan Data Pernerbangan Awal', 404);
    }

    public function pesawatKembali(Request $request)
    {
        $flight = Api\Tiket::pesawatKembali($request);

        if (
            !is_null($flight)
            && array_key_exists('departures', $flight)
            && array_key_exists('result', $flight['departures']) 
            && isset($flight['departures']['result'])
        ) {
            $result = $flight['departures']['result'];
            $prices = array_column($result, 'price_value');
            $min = $result[array_search(min($prices), $prices)];
            
            $termurah='
                <div class="item" id="'. $min['flight_id'] .'">
                    <div class="ui tiny image">
                        <img src="'. $min['image'].'" alt="'. $min['airlines_name'].' Logo">
                    </div>
                    <div class="content">
                        <div class="header" data-tooltip="'. $min['duration'].'" data-inverted>
                            '. $min['simple_departure_time'].' - '. $min['simple_arrival_time'].'
                        </div>
                        <div class="meta">
                            <span class="departure">'. $min['departure_city_name'].' ('. $min['departure_city'].')</span>
                            <span>-</span>
                            <span class="arrival">'. $min['arrival_city_name'].' ('. $min['arrival_city'].')</span>
                            <span class="flight_number">'. $min['flight_number'].'</span>
                            <span class="price selected pulang">Rp ' . number_format($min['price_value'],0,'.',',').'</span>
                        </div>
                    </div>
                </div>
            ';
            for ($i=0; $i<count($result); $i++):
                $item[$i]='
                    <div class="item" id="'. $result[$i]['flight_id'] .'">
                        <div class="ui tiny image">
                            <img src="'. $result[$i]['image'].'" alt="'. $result[$i]['airlines_name'].' Logo">
                        </div>
                        <div class="content">
                            <div class="header" data-tooltip="'. $result[$i]['duration'].'" data-inverted>
                                '. $result[$i]['simple_departure_time'].' - '. $result[$i]['simple_arrival_time'].'
                            </div>
                            <div class="meta">
                                <span class="departure">'. $result[$i]['departure_city_name'].' ('. $result[$i]['departure_city'].')</span>
                                <span>-</span>
                                <span class="arrival">'. $result[$i]['arrival_city_name'].' ('. $result[$i]['arrival_city'].')</span>
                                <span class="flight_number">'. $result[$i]['flight_number'].'</span>
                                <span class="price">Rp ' . number_format($result[$i]['price_value'],0,'.',',').'</span>
                            </div>
                            <div class="extra">
                                <div class="ui right floated primary button" onclick="pilihPesawat(\'pulang\',\''. $result[$i]['flight_id'] .'\')">
                                    Pilih
                                    <i class="right plane icon"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                ';
            endfor;
            $item=implode($item);

            return response()->json(['min' => $termurah, 'full' => $item], 200);
        }

        return response('Gagal Mendapatkan Data Pernerbangan Kembali', 404);
    }

    public function hotel(Request $request)
    {
        $start = Carbon::parse($request->session()->get('parameter.start'));
        $end = Carbon::parse($request->session()->get('parameter.end'));
        $durasi = $end->diffInDays($start);

        $dana = str_replace('.', '', session('parameter.dana'));
        $pesawat = intval(session('biaya.pesawat'));
        $objekWisata = intval(session('biaya.objekWisata.total'));
        $mobil = intval(session('biaya.mobil'));
        $alokasi = ((($dana - $pesawat) - $objekWisata) - $mobil) / $durasi;

        $tujuans = Area::where('area', session('parameter.tujuan'))->get();

        return view('cari.hotel', compact('alokasi', 'start', 'end', 'durasi', 'tujuans'));
    }

    public function getHotel(Request $request, $detail = null)
    {
        if (!is_null($detail)) {
            if ($request->session()->has('urlDetailHotel')) {
                $request->session()->forget('urlDetailHotel');
            }

            $request->session()->put('urlDetailHotel', $request->input('url'));
            return response('', 204);
        }

        $hotel = Api\Tiket::getHotel($request);
        if (isset($hotel['results']['result']) && count($hotel['results']['result']) > 0) {
            $result=$hotel['results']['result'];
            for ($i = 0; $i < count($result); $i++) {
            $item[$i]='
                <div class="item" id="'.$result[$i]['id'].'">
                    <div class="ui small rounded image">
                        <img src="'.str_replace('sq2','sq3',$result[$i]['photo_primary']).'" alt="'.$result[$i]['name'].' Logo">
                    </div>
                    <div class="content">
                        <div class="header">
                            '.$result[$i]['name'].'
                        </div>
                        <div class="meta">
                            <span class="rating">
                                <div class="ui star rating" data-rating="'. $result[$i]['star_rating'].'"></div>
                            </span>
                            <span class="regional">'.$result[$i]['address'].'</span>
                            <span>
                                <span class="price">Rp '.number_format($result[$i]['price'],0,'.',',').'</span> / Malam
                            </span>
                        </div>
                        <p>'.str_replace('|',' | ',$result[$i]['room_facility_name']).'</p>
                        <div class="extra">
                            <span class="hide lat">
                                '.$result[$i]['latitude'].'
                            </span>
                            <span class="hide lng">
                                '.$result[$i]['longitude'].'
                            </span>
                            <a class="ui right floated primary detail button" data-url="'.$result[$i]['business_uri'].'">
                                Detail
                                <i class="right hotel icon"></i>
                            </a>
                        </div>
                    </div>
                </div>
            ';
            }
            $item=implode($item);

            return response()->json($item, 200);
        } else {
            return response('Data Hotel Tidak Ditemukan.', 404);
        }
    }

    public function detailHotel(Request $request)
    {
        if (!$request->session()->has('urlDetailHotel')) {
            return redirect()->back();
        }

        $url = $request->session()->get('urlDetailHotel');
        $url = $url.'&token='.$request->session()->get('token.hotel').'&output=json';
        $hotel = Api\Tiket::detailHotel($url);

        $start = Carbon::parse($request->session()->get('parameter.start'));
        $end = Carbon::parse($request->session()->get('parameter.end'));
        $durasi = $end->diffInDays($start);

        $dana = str_replace('.', '', $request->session()->get('parameter.dana'));
        $pesawat = intval($request->session()->get('biaya.pesawat'));
        $objekWisata = intval($request->session()->get('biaya.objekWisata'));
        $mobil = intval($request->session()->get('biaya.mobil'));
        $dana = ((($dana - $pesawat) - $objekWisata) - $mobil) / $durasi;

        return view('cari.hotelDetail', compact('hotel', 'dana', 'start', 'end'));
    }

    public function objekWisata(Request $request, $id = null)
    {
        if ($id !== null) {
            $diganti = $request->session()->put('objekWisata.diganti', intval($id));
        }

        $area = $request->session()->get('parameter.tujuan');
        $selected = $request->session()->get('objekWisata');

        $objekWisatas = ObjekWisata::with(['area', 'images', 'detailTipe.tipe'])
            ->whereHas('area', function ($query) use ($area) {
                $query->where('area', $area);
            })
            ->where(function ($query) use ($selected) {
                if (!is_null($selected)) {
                    $query->whereNotIn('id', $selected);
                }
            })
            ->get();

        return view('cari.objekWisata', compact('objekWisatas'));
    }

    public function detailObjekWisata(Request $request, $id)
    {
        $dana = str_replace('.', '', $request->session()->get('parameter.dana'));
        $pesawat = intval($request->session()->get('biaya.pesawat'));
        $objekWisata = intval($request->session()->get('biaya.objekWisata'));
        $mobil = intval($request->session()->get('biaya.mobil'));
        $hotel = intval($request->session()->get('biaya.hotel'));
        $sisaDana = ((($dana - $pesawat) - $hotel) - $objekWisata) - $mobil;

        $objekWisatas = ObjekWisata::with(['area', 'images', 'detailTipe.tipe'])
            ->where('id', $id)
            ->get();

        return view('cari.objekWisataDetail', compact('objekWisatas', 'sisaDana'));
    }

    public function mobil(Request $request)
    {
        $dana = str_replace('.', '', $request->session()->get('parameter.dana'));
        $pesawat = intval($request->session()->get('biaya.pesawat'));
        $objekWisata = intval($request->session()->get('biaya.objekWisata'));
        $hotel = intval($request->session()->get('biaya.hotel'));
        $sisaDana = (($dana - $pesawat) - $hotel) - $objekWisata;

        $selected = $request->session()->get('mobil');

        $mobils = Mobil::where('harga_kosongan', '<', $sisaDana)
            ->where(function ($query) use ($selected) {
                if (!is_null($selected)) {
                    $query->whereNotIn('id', $selected);
                }
            })
            ->get();
        $minPrice = Mobil::min('harga_kosongan');

        return view('cari.mobil', compact('sisaDana', 'mobils', 'minPrice'));
    }

    public function simpan(Request $request)
    {
        try {
            DB::beginTransaction();

            $session = $request->session()->all();

            $area = Area::where('area', $session['parameter']['tujuan'])->first();

            $transaksi = new Transaksi();
            $transaksi->id_user = Auth::user()->id;
            $transaksi->id_area = $area->id;
            $transaksi->tgl_transaksi = Carbon::now()->format('Y-m-d');
            $transaksi->asal = array_get($session, 'parameter.asal');
            $transaksi->dana = intval(array_get($session, 'parameter.dana'));
            $transaksi->start = Carbon::parse(array_get($session, 'parameter.start'))->format('Y-m-d');
            $transaksi->end = Carbon::parse(array_get($session, 'parameter.end'))->format('Y-m-d');
            $transaksi->dewasa = intval(array_get($session, 'parameter.dewasa'));
            $transaksi->anak = intval(array_get($session, 'parameter.anak'));
            $transaksi->batita = intval(array_get($session, 'parameter.batita'));
            $transaksi->order_id_pesawat = array_get($session, 'order.pesawat.myorder.order_id');
            $transaksi->order_id_hotel = array_get($session, 'order.hotel.myorder.order_id');
            $transaksi->token_pesawat = array_get($session, 'token.pesawat');
            $transaksi->token_hotel = array_get($session, 'token.hotel');
            $transaksi->save();

            $hotel = new TransaksiHotel();
            $hotel->id_transaksi = $transaksi->id;
            $hotel->order_name = array_get($session, 'order.hotel.myorder.data.0.order_name');
            $hotel->order_name_detail = array_get($session, 'order.hotel.myorder.data.0.order_name_detail');
            $hotel->link_gambar = array_get($session, 'order.hotel.myorder.data.0.order_photo');
            $hotel->lat = array_get($session, 'koordinat.hotel.lat');
            $hotel->lng = array_get($session, 'koordinat.hotel.lng');
            $hotel->harga = array_get($session, 'order.hotel.myorder.total_without_tax');
            $hotel->harga_plus_pajak = array_get($session, 'order.hotel.myorder.total');
            $hotel->save();

            if (array_key_exists('mobil', $session)) {
                $mobil = new TransaksiMobil();
                $mobil->id_transaksi = $transaksi->id;
                $mobil->id_mobil = array_get($session, 'mobil.id');
                $mobil->supir = array_get($session, 'mobil.supir');
                $mobil->save();
            }

            if (array_key_exists('objekWisata', $session)) {
                if (array_has($session, 'objekWisata.diganti')) {
                    array_forget($session, 'objekWisata.diganti');
                }
    
                for ($i = 0; $i < count($session['objekWisata']); $i++) { 
                    $objekWisata = new TransaksiObjekWisata();
                    $objekWisata->id_transaksi = $transaksi->id;
                    $objekWisata->id_objek_wisata = array_get($session, 'objekWisata.'.$i);
                    $objekWisata->save();
                }
            }

            $pesawat = new TransaksiPesawat();
            $pesawat->id_transaksi = $transaksi->id;
            $pesawat->order_name = array_get($session, 'order.pesawat.myorder.data.0.order_name');
            $pesawat->order_name_detail = array_get($session, 'order.pesawat.myorder.data.0.order_name_detail');
            $pesawat->logo = array_get($session, 'order.pesawat.myorder.data.0.order_photo');
            $pesawat->harga = intval(array_get($session, 'order.pesawat.myorder.data.0.detail.price'));
            $pesawat->harga_plus_pajak = intval(array_get($session, 'order.pesawat.myorder.data.0.detail.subtotal_and_charge'));
            $pesawat->tgl_berangkat = Carbon::parse(array_get($session, 'waktu.berangkat.tgl'))->format('Y-m-d');
            $pesawat->jam_berangkat = array_get($session, 'waktu.berangkat.jamBerangkat');
            $pesawat->jam_sampai = array_get($session, 'waktu.berangkat.jamSampai');
            $pesawat->pulang_pergi = 'pergi';
            $pesawat->save();

            $pesawat = new TransaksiPesawat();
            $pesawat->id_transaksi = $transaksi->id;
            $pesawat->order_name = array_get($session, 'order.pesawat.myorder.data.1.order_name');
            $pesawat->order_name_detail = array_get($session, 'order.pesawat.myorder.data.1.order_name_detail');
            $pesawat->logo = array_get($session, 'order.pesawat.myorder.data.1.order_photo');
            $pesawat->harga = intval(array_get($session, 'order.pesawat.myorder.data.1.detail.price'));
            $pesawat->harga_plus_pajak = intval(array_get($session, 'order.pesawat.myorder.data.1.detail.subtotal_and_charge'));
            $pesawat->tgl_berangkat = Carbon::parse(array_get($session, 'waktu.kembali.tgl'))->format('Y-m-d');
            $pesawat->jam_berangkat = array_get($session, 'waktu.kembali.jamBerangkat');
            $pesawat->jam_sampai = array_get($session, 'waktu.kembali.jamSampai');
            $pesawat->pulang_pergi = 'pulang';
            $pesawat->save();

            for ($i = 0; $i < count($session['dataWisatawan']); $i++) { 
                $wisatawan = new TransaksiWisatawan();
                $wisatawan->id_transaksi = $transaksi->id;
                $wisatawan->title = array_get($session, 'dataWisatawan.'.$i.'.title');
                $wisatawan->nama_depan = array_get($session, 'dataWisatawan.'.$i.'.namaDepan');
                $wisatawan->nama_belakang = array_get($session, 'dataWisatawan.'.$i.'.namaBelakang');
                $wisatawan->tgl_lahir = Carbon::parse(array_get($session, 'dataWisatawan.'.$i.'.tglLahir'))->format('Y-m-d');
                $wisatawan->save();
            }

            DB::commit();
            return response($transaksi->id, 200);
        } catch (\Exception $ex) {
            DB::rollBack();
            dd($ex);
            return response()->json($ex->getMessage(), 500);
        }
    }
}
