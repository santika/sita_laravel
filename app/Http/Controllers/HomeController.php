<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Area;
use App\Models\Mobil;
use App\Models\ObjekWisata;
use App\Models\TipeObjekWisata;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $areas = Area::all();
        $tipes = TipeObjekWisata::all();
        $airports = Api\Tiket::airports($request);

        if (!$request->session()->has('koordinat.airport')) {
            $request->session()->put('koordinat.airport', '-8.7467172,115.166787');
        }

        return view('index', compact('airports', 'areas', 'tipes'));
    }

    public function simpanParameter(Request $request)
    {
        if ($request->type == 'manual') {
            $data['parameter']['type'] = 'manual';
            $data['parameter']['asal'] = $request->asal;
            $data['parameter']['tujuan'] = $request->tujuan;
            $data['parameter']['dana'] = $request->dana;
            $data['parameter']['start'] = $request->start;
            $data['parameter']['end'] = $request->end;
            $data['parameter']['dewasa'] = $request->dewasa;
            $data['parameter']['anak'] = $request->anak;
            $data['parameter']['batita'] = $request->batita;
        } elseif ($request->type == 'otomatis') {
            $data['parameter']['type'] = 'otomatis';
            $data['parameter']['asal'] = $request->asal;
            $data['parameter']['tujuan'] = $request->tujuan;
            $data['parameter']['dana'] = $request->dana;
            $data['parameter']['start'] = $request->start;
            $data['parameter']['end'] = $request->end;
            $data['parameter']['dewasa'] = $request->dewasa;
            $data['parameter']['anak'] = $request->anak;
            $data['parameter']['batita'] = $request->batita;
            $data['parameter']['tipe'] = $request->tipe;
            $data['parameter']['mobil'] = intval($request->mobil);
        }

        $request->session()->put($data);

        return response()->json('', 200);
    }

    public function simpanPesawat(Request $request)
    {
        $order = Api\Tiket::orderFlight($request);
        if ($order['diagnostic']['status'] != 200) {
            return response()->json($order['diagnostic']['error_msgs'], 400);
        }

        $cek = Api\Tiket::cekOrder($request, 'pesawat');
        if ($cek['diagnostic']['status'] !== 200 || !array_key_exists('myorder', $cek)) {
            return response()->json('Gagal Melakukan Pemesanan', 400);
        }

        $dana = $request->session()->get('parameter.dana');
        $biaya = $cek['myorder']['total'];

        $request->session()->put('biaya.pesawat', $biaya);
        $request->session()->put('order.pesawat', $cek);

        $waktu['berangkat']['tgl']=$cek['myorder']['data'][0]['detail']['flight_date'];
        $waktu['berangkat']['jamBerangkat']=$cek['myorder']['data'][0]['detail']['departure_time'];
        $waktu['berangkat']['jamSampai']=$cek['myorder']['data'][0]['detail']['arrival_time'];
        $waktu['kembali']['tgl']=$cek['myorder']['data'][1]['detail']['flight_date'];
        $waktu['kembali']['jamBerangkat']=$cek['myorder']['data'][1]['detail']['departure_time'];
        $waktu['kembali']['jamSampai']=$cek['myorder']['data'][1]['detail']['arrival_time'];

        $request->session()->put('waktu', $waktu);

        return response('', 200);
    }

    public function simpanHotel(Request $request)
    {
        $hotel = ['lat' => $request->input('lat'), 'lng' => $request->input('lng')];
        $request->session()->put('koordinat.hotel', $hotel);

        $start = $request->session()->get('waktu.berangkat.tgl').' '.$request->session()->get('waktu.berangkat.jamSampai');
        $end = $request->session()->get('waktu.kembali.tgl'). ' '.$request->session()->get('waktu.kembali.jamBerangkat');
        $cariDurasi = Api\Map::getDurationAndDistance($request, 'airport', $hotel['lat'].','.$hotel['lng'], strtotime($start));

        $end = Carbon::parse($end);
        $lamaTravel = Carbon::parse($start);
        $durasi = $cariDurasi['routes'][0]['legs'][0]['duration_in_traffic']['value'] + 7200;
        $lamaTravel->addSeconds($durasi);

        if ($lamaTravel->diffInHours($end, false) <= 5) {
            return response()->json('Waktu Tidak Mencukupi.', 400);
        }

        $order = Api\Tiket::orderHotel($request, $request->input('u').'&token='.$request->session()->get('token.hotel').'&output=json');
        $cek = Api\Tiket::cekOrder($request, 'hotel');

        if ($order['diagnostic']['status'] != '200') {
            return response()->json($order['diagnostic']['error_msgs'], 400);
        }

        $request->session()->put('order.hotel', $cek);
        $request->session()->put('durasi.hotel', $durasi);
        $request->session()->put('biaya.hotel', intval(number_format($cek['myorder']['data'][0]['subtotal_and_charge'], 0, '', '')));
        $request->session()->forget('urlDetailHotel');

        return response()->json('', 200);
    }

    public function simpanObjekWisata(Request $request)
    {
        $id = intval($request->input('id'));
        $biaya = intval($request->input('dewasa')) + intval($request->input('anak'));
        $koordinat['latlng'] = $request->input('latlng');

        $start = $request->session()->get('waktu.berangkat.tgl').' '.$request->session()->get('waktu.berangkat.jamSampai');
        $hotel = $request->session()->get('koordinat.hotel');

        $cariDurasi = Api\Map::getDurationAndDistance($request, $hotel['lat'].','.$hotel['lng'], $request->input('latlng'), strtotime($start));

        $durasi = intval($request->session()->get('durasi.hotel'));

        if ($request->session()->has('durasi.objekWisata')) {
            $durasiObjekwisata = $request->session()->get('durasi.objekWisata');

            foreach ($durasiObjekwisata as $objekWisata) {
                $durasi = $durasi + $objekWisata;
            }
        }

        $start = Carbon::parse($start);
        $end = Carbon::parse($request->session()->get('waktu.kembali.tgl').' '.$request->session()->get('waktu.kembali.jamBerangkat'));
        $start->addSeconds($durasi);

        if ($start->diffInHours($end, false) <= 5) {
            return response()->json('Waktu Tidak Mencukupi.', 400);
        }

        if ($request->session()->has('objekWisata.diganti')) {
            $diganti = intval($request->session()->pull('objekWisata.diganti'));
            $terpilih = $request->session()->get('objekWisata');
            $index = array_search($diganti, $terpilih);
            $request->session()->put('objekWisata.'.$index, $id);
            $request->session()->put('biaya.objekWisata.'.$index, $biaya);
            $request->session()->put('koordinat.objekWisata.'.$index, $koordinat);
        } else {
            $request->session()->push('objekWisata', $id);
            $request->session()->push('biaya.objekWisata', $biaya);
            $request->session()->push('koordinat.objekWisata', $koordinat);
        }

        return response('', 200);
    }

    public function simpanMobil(Request $request)
    {
        $idMobil = intval($request->input('idMobil'));
        $harga = intval($request->input('harga'));
        $supir = $request->input('supir');

        $request->session()->put('mobil.id', $idMobil);
        $request->session()->put('mobil.supir', $supir);
        $request->session()->put('biaya.mobil', $harga);
        
        return response()->json('', 200);
    }

    public function deleteObjekWisata(Request $request)
    {
        $request->session()->pull('koordinat.objekWisata');
        $request->session()->forget('objekWisata');
        $request->session()->pull('biaya.objekWisata');

        dump($request->session()->all());
    }

    public function rekap(Request $request)
    {
        $request->session()->forget('objekWisata.diganti');

        $parameters = $request->session()->get('parameter');
        $token = $request->session()->get('token');
        $order = $request->session()->get('orde');
        $objekWisata = $request->session()->get('objekwisata');
        $mobil = $request->session()->get('mobil');

        $dana = str_replace('.', '', $request->session()->get('parameter.dana'));
        $pesawat = intval($request->session()->get('biaya.pesawat'));
        $mobil = intval($request->session()->get('biaya.mobil'));
        $hotel = intval($request->session()->get('biaya.hotel'));
        $totalBiaya = $pesawat + $hotel + $objekWisata + $mobil;
        $sisaDana = $dana - $totalBiaya;

        if ($request->session()->has('objekWisata')) {
            $idObjekWisata = $request->session()->get('objekWisata');
            $objekWisatas = ObjekWisata::with(['area', 'images', 'detailTipe.tipe'])
                ->where(function ($query) use ($idObjekWisata) {
                    for ($i = 0; $i < count($idObjekWisata); $i++) { 
                        if ($i === 0) {
                            $query->where('id', $idObjekWisata[$i]);
                        } else {
                            $query->orWhere('id', $idObjekWisata[$i]);
                        }
                        
                    }
                })
                ->get();

            $jumlahObjekWisata = ObjekWisata::with(['area'])
                ->whereHas('area', function ($query) use ($parameters) {
                    $query->where('area', $parameters['tujuan']);
                })
                ->count();
        }

        if ($request->session()->has('mobil')) {
            $idMobil = intval($request->session()->get('mobil.id'));
            $mobils = Mobil::where('id', $idMobil)->get();
        }

        return view('rekap', compact('parameters', 'token', 'order', 'objekWisatas', 'jumlahObjekWisata', 'mobil', 'mobils', 'sisaDana', 'totalBiaya'));
    }

    public function tambahWisatawan(Request $request)
    {
        $parameters = $request->session()->get('parameter');

        return view('tambahwisatawan', compact('parameters'));
    }

    public function tambahWisatawanSimpan(Request $request)
    {
        $input = $request->all();

        for ($i=0; $i < $request->input('jumlah'); $i++) { 
            $data[$i]['title'] = $input['title'][$i];
            $data[$i]['namaDepan'] = $input['nd'][$i];
            $data[$i]['namaBelakang'] = $input['nb'][$i];
            $data[$i]['tglLahir'] = $input['tgl'][$i];
        }

        $request->session()->put('dataWisatawan', $data);
        
        return response()->json('', 200);
    }
}
