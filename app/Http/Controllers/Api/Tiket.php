<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Area;

class Tiket extends Controller
{
    public static function tokenPesawat(Request $request)
    {
        if (!$request->session()->has('token.pesawat')) {
            $requestToken = \Curl::to('https://api-sandbox.tiket.com/apiv1/payexpress')
                ->withData(['method'=>'getToken','secretkey'=>'33fc5abcb49329e6169c72c62576d447','output'=>'json'])
                ->get();

            $token = json_decode($requestToken, true);
            $request->session()->put('token.pesawat', $token['token']);
        }
    }

    public static function tokenHotel(Request $request)
    {
        if (!$request->session()->has('token.hotel')) {
            $requestToken = \Curl::to('https://api-sandbox.tiket.com/apiv1/payexpress')
                ->withData(['method'=>'getToken','secretkey'=>'33fc5abcb49329e6169c72c62576d447','output'=>'json'])
                ->get();

            $token = json_decode($requestToken, true);
            $request->session()->put('token.hotel', $token['token']);
        }
    }

    public static function airports(Request $request)
    {
        self::tokenPesawat($request);

        if (\Cache::has('airports') == null) {
            $airports = \Curl::to('https://api-sandbox.tiket.com/flight_api/all_airport')
                ->withData(['token'=>$request->session()->get('token.pesawat'),'depart'=>'DPS','output'=>'json'])
                ->get();
            $airports = json_decode($airports, true);
            \Cache::forever('airports', $airports['all_airport']['airport']);
        }

        return \Cache::get('airports');
    }

    public static function pesawatAwal(Request $request)
    {
        $date = new \DateTime($request->session()->get('parameter.start'));
        $payload = [
            'token' => $request->session()->get('token.pesawat'),
            'd' => $request->session()->get('parameter.asal'),
            'a' => 'DPS',
            'date' => $date->format('Y-m-d'),
            'adult' => $request->session()->get('parameter.dewasa'),
            'child' => $request->session()->get('parameter.anak'),
            'infant' => $request->session()->get('parameter.batita'),
            'v'=>'2','output'=>'json'
        ];

        $requestFlight = \Curl::to('https://api-sandbox.tiket.com/search/flight')
            ->withData($payload)
            ->get();
        
        return json_decode($requestFlight, true);
    }

    public static function pesawatKembali(Request $request)
    {
        $date = Carbon::parse($request->session()->get('parameter.end'));
        $payload = [
            'token' => $request->session()->get('token.pesawat'),
            'd' => 'DPS',
            'a' => $request->session()->get('parameter.asal'),
            'date' => $date->format('Y-m-d'),
            'adult' => $request->session()->get('parameter.dewasa'),
            'child' => $request->session()->get('parameter.anak'),
            'infant' => $request->session()->get('parameter.batita'),
            'v'=>'2','output'=>'json'
        ];

        $requestFlight = \Curl::to('https://api-sandbox.tiket.com/search/flight')
            ->withData($payload)
            ->get();

        return json_decode($requestFlight, true);
    }

    public static function cekOrder(Request $request, $tipe)
    {
        if ($tipe == 'pesawat') {
            $cek = \Curl::to('https://api-sandbox.tiket.com/order')
                ->withData(['token'=>$request->session()->get('token.pesawat'),'output'=>'json'])
                ->get();

            return json_decode($cek, true);
        }

        if ($tipe == 'hotel') {
            $cek = \Curl::to('https://api-sandbox.tiket.com/order')
                ->withData(['token'=>$request->session()->get('token.hotel'),'output'=>'json'])
                ->get();

            return json_decode($cek, true);
        }
    }

    public static function deleteOrder(Request $request, $tipe, $url)
    {
        if ($tipe == 'pesawat') {
            $delete = \Curl::to($url.'&token='.$request->session()->get('token.pesawat').'&output=json')
                ->get();

            return json_decode($delete, true);
        }
        if ($tipe == 'hotel') {
            $delete = \Curl::to($url.'&token='.$request->session()->get('token.hotel').'&output=json')
                ->get();

            return json_decode($delete, true);
        }
    }

    public static function orderFlight(Request $request, $depart = false, $return = false)
    {
        $order = self::cekOrder($request, 'pesawat');

        if ($order['diagnostic']['status'] === 200 || array_key_exists('myorder', $order)) {
            for ($i = 0; $i < count($order['myorder']['data']); $i++) {
                if ($order['myorder']['data'][$i]['order_type'] === 'flight') {
                    self::deleteOrder($request, 'pesawat', $order['myorder']['data'][$i]['delete_uri']);
                }
            }
        }

        $wisatawan = $request->session()->get('dataWisatawan');
        $jumlahWisatawan = count($wisatawan);
        $parameter = $request->session()->get('parameter');
        $token = $request->session()->get('token.pesawat');
        $user = \Auth::user();
        $datawisatawan = [];

        for ($i = 0; $i < $jumlahWisatawan; $i++) {
            $j = $i + 1;

            if ($wisatawan[$i]['title'] == 'Mstra') {
                $textfirstname = 'firstnamec'.$j;
                $textlastname = 'lastnamec'.$j;
                $texttitle = 'titlec'.$j;
                $valuetitle = 'Mstr';
                $textbirthday = 'birthdatec'.$j;
            } elseif ($wisatawan[$i]['title'] == 'Missa') {
                $textfirstname = 'firstnamec'.$j;
                $textlastname = 'lastnamec'.$j;
                $texttitle = 'titlec'.$j;
                $valuetitle = 'Miss';
                $textbirthday = 'birthdatec'.$j;
            } elseif ($wisatawan[$i]['title'] == 'Mstrb') {
                $textfirstname = 'firstnamei'.$j;
                $textlastname = 'lastnamei'.$j;
                $texttitle = 'titlei'.$j;
                $valuetitle = 'Mstr';
                $textbirthday = 'birthdatei'.$j;
            } elseif ($wisatawan[$i]['title'] == 'Missb') {
                $textfirstname = 'firstnamei'.$j;
                $textlastname = 'lastnamei'.$j;
                $texttitle = 'titlei'.$j;
                $valuetitle = 'Miss';
                $textbirthday = 'birthdatei'.$j;
            } else {
                $textfirstname = 'firstnamea'.$j;
                $textlastname = 'lastnamea'.$j;
                $texttitle = 'titlea'.$j;
                $valuetitle = $wisatawan[$i]['title'];
                $textbirthday = 'birthdatea'.$j;
            }

            $wisatawan = [
                $textfirstname => $wisatawan[$i]['namaDepan'],
                $textlastname => $wisatawan[$i]['namaBelakang'],
                $texttitle => $valuetitle,
                $textbirthday => date("Y-m-d", strtotime($wisatawan[$i]['tglLahir']))
            ];

            $datawisatawan=array_merge($datawisatawan,$wisatawan);
        }

        $variable = [
            'token' => $token,
            'adult' => $parameter['dewasa'],
            'child' => $parameter['anak'],
            'infant' => $parameter['batita'],
            'conSalutation' => $user->title,
            'conFirstName' => $user->nama_depan,
            'conLastName' => $user->nama_belakang,
            'conPhone' => $user->no_telpon,
            'conEmailAddress' => $user->email,
            'output' => 'json'
        ];

        if ($request->session()->get('parameter.type') === 'manual') {
            $variable = array_add($variable, 'flight_id', $request->input('idPergi'));
            $variable = array_add($variable, 'ret_flight_id', $request->input('idPulang'));
        } elseif ($request->session()->get('parameter.type') === 'otomatis') {
            $variable = array_add($variable, 'flight_id', $depart);
            $variable = array_add($variable, 'ret_flight_id', $return);
        }

        $variable = array_merge($variable,$datawisatawan);

        $order = \Curl::to('https://api-sandbox.tiket.com/order/add/flight')
            ->withData($variable)
            ->get();

        return json_decode($order, true);
    }

    public static function getHotel(Request $request, $sort = false)
    {
        self::tokenHotel($request);

        $start = Carbon::parse($request->session()->get('parameter.start'));
        $end = Carbon::parse($request->session()->get('parameter.end'));
        $durasi = $end->diffInDays($start);

        $tujuans = Area::where('area', session('parameter.tujuan'))->get();

        foreach ($tujuans as $tujuan) {
            $area = $tujuan->area;
            $lat = $tujuan->lat;
            $lng = $tujuan->lng;
        }

        $dana = str_replace('.', '', session('parameter.dana'));
        $pesawat = intval(session('biaya.pesawat'));
        $objekWisata = intval(session('biaya.objekWisata.total'));
        $mobil = intval(session('biaya.mobil'));
        $alokasi = ((($dana - $pesawat) - $objekWisata) - $mobil) / $durasi;

        $payload = [
            'token' => $request->session()->get('token.hotel'),
            'q' => $area,
            'night' => $durasi,
            'startdate' => $start->format('Y-m-d'),
            'enddate' => $end->format('Y-m-d'),
            'latitude' => $lat,
            'longitude' => $lng,
            'adult' => intval($request->session()->get('parameter.dewasa')),
            'child' => intval($request->session()->get('parameter.anak')),
            'room' => ceil((intval($request->session()->get('parameter.dewasa')) + intval($request->session()->get('parameter.anak'))) / 2),
            'maxprice' => $alokasi,
            'output' => 'json'
        ];

        if ($sort) {
            $payload = array_add($payload, 'sort', 'priceasc');
        }

        $get = \Curl::to('https://api-sandbox.tiket.com/search/hotel')
            ->withData($payload)
            ->get();
        
        return json_decode($get, true);
    }

    public static function detailHotel($url)
    {
        $get = \Curl::to($url)
            ->get();

        return json_decode($get, true);
    }

    public static function orderHotel(Request $request, $url)
    {
        $order = self::cekOrder($request, 'hotel');

        if ($order['diagnostic']['status'] === 200 || is_array($order) && array_key_exists('myorder', $order)) {
            for ($i = 0;$i < count($order['myorder']['data']); $i++) {
                if ($order['myorder']['data'][$i]['order_type'] === 'hotel') {
                    self::deleteOrder($request, 'hotel', $order['myorder']['data'][$i]['delete_uri']);
                }
            }
        }

        $order = \Curl::to($url)
            ->get();

        return json_decode($order, true);
    }
}
