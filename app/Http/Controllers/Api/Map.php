<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Map extends Controller
{
    private const KEY = 'AIzaSyChrdDRWwH5tZRc_wDGUGT8gjcTrEIJEns';
    private const AIRPORT = '-8.7467172,115.166787';

    public static function getDurationAndDistance(Request $request, $a,$b,$departTime)
    {
        if ($a == 'airport') {
            $a = self::AIRPORT;
        }
        if ($b == 'airport') {
            $b = self::AIRPORT;
        }

        $payload = [
            'origin' => $a,
            'destination' => $b,
            'departure_time' => $departTime,
            'traffic_model' => 'pessimistic',
            'language' => 'id', 'key' => self::KEY
        ];

        $get = \Curl::to('https://maps.googleapis.com/maps/api/directions/json')
            ->withData($payload)
            ->get();

        return json_decode($get, true);
    }
}
