<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public static function tokenPesawat(Request $request)
    {
        if (!$request->session()->has('token.pesawat')) {
            $requestToken = \Curl::to('https://api-sandbox.tiket.com/apiv1/payexpress')
                ->withData(['method'=>'getToken','secretkey'=>'33fc5abcb49329e6169c72c62576d447','output'=>'json'])
                ->get();

            $token = json_decode($requestToken, true);
            $request->session()->push('token.pesawat', $token['token']);
        }
    }

    public static function tokenHotel(Request $request)
    {
        if (!$request->session()->has('token.hotel')) {
            $requestToken = \Curl::to('https://api-sandbox.tiket.com/apiv1/payexpress')
                ->withData(['method'=>'getToken','secretkey'=>'33fc5abcb49329e6169c72c62576d447','output'=>'json'])
                ->get();

            $token = json_decode($requestToken, true);
            $request->session()->push('token.hotel', $token['token']);
        }
    }

    public static function airports(Request $request)
    {
        ApiController::tokenPesawat($request);

        if (\Cache::has('airports') == null) {
            $airports = \Curl::to('https://api-sandbox.tiket.com/flight_api/all_airport')
                ->withData(['token'=>$request->session()->get('token.pesawat'),'depart'=>'DPS','output'=>'json'])
                ->get();
            $airports = json_decode($airports, true);
            \Cache::forever('airports', $airports['all_airport']['airport']);
        }

        return \Cache::get('airports');
    }

    public static function pesawatAwal(Request $request)
    {
        $requestFlight = \Curl::to('https://api-sandbox.tiket.com/search/flight')
            ->withData([
                'token'=>session('token.pesawat'),
                'd'=>$request->input('bandara'),
                'a'=>'DPS',
                'date'=>$request->input('date'),
                'adult'=>$request->input('dewasa'),
                'child'=>$request->input('anak'),
                'infant'=>$request->input('batita'),
                // 'sort' => true,
                'v'=>'1','output'=>'json'
            ])
            ->get();
        
        return json_decode($requestFlight, true);
    }

    public static function pesawatKembali(Request $request)
    {
        $requestFlight = \Curl::to('https://api-sandbox.tiket.com/search/flight')
            ->withData([
                'token'=>session('token.pesawat'),
                'd'=>'DPS',
                'a'=>$request->input('bandara'),
                'date'=>$request->input('date'),
                'adult'=>$request->input('dewasa'),
                'child'=>$request->input('anak'),
                'infant'=>$request->input('batita'),
                // 'sort' => true,
                'v'=>'1','output'=>'json'
            ])
            ->get();

        return json_decode($requestFlight, true);
    }

    public static function cekOrder(Request $request, $tipe)
    {
        if ($tipe == 'pesawat') {
            $cek = \Curl::to('https://api-sandbox.tiket.com/order')
                ->withData(['token'=>$request->session()->get('token.pesawat'),'output'=>'json'])
                ->get();

            return json_decode($cek, true);
        }

        if ($tipe == 'hotel') {
            $cek = \Curl::to('https://api-sandbox.tiket.com/order')
                ->withData(['token'=>$request->session()->get('token.hotel'),'output'=>'json'])
                ->get();

            return json_decode($cek, true);
        }
    }

    public static function deleteOrder(Request $request, $tipe, $url)
    {
        if ($tipe == 'pesawat') {
            $delete = \Curl::to($url.'&token='.$request->session()->get('token.pesawat').'&output=json')
                ->get();

            return json_decode($delete, true);
        }
        if ($tipe == 'hotel') {
            $delete = \Curl::to($url.'&token='.$request->session()->get('token.hotel').'&output=json')
                ->get();

            return json_decode($delete, true);
        }
    }

    public static function orderFlight(Request $request)
    {
        $order = ApiController::cekOrder($request, 'pesawat');
        
        if (isset($order['myorder']['data'])) {
            $jumlah=0;
            for ($i=0;$i<count($order['myorder']['data']);$i++) {
                if ($order['myorder']['data'][$i]['order_type']=='flight') {
                    $jumlah++;
                }
            }
            if ($jumlah>=2) {
                for ($i=0;$i<count($order['myorder']['data']);$i++) {
                    if ($order['myorder']['data'][$i]['order_type']=='flight') {
                        ApiController::deleteOrder($request, 'pesawat', $order['myorder']['data'][$i]['delete_uri']);
                    }
                }
            }
        }

        $wisatawan = $request->session()->get('dataWisatawan');
        $jumlahWisatawan = count($wisatawan);
        $parameter = $request->session()->get('parameter');
        $token = $request->session()->get('token.pesawat');
        $user = \Auth::user();
        $datawisatawan = [];

        for ($i = 0; $i < $jumlahWisatawan; $i++) {
            $j = $i + 1;

            if ($wisatawan[$i]['title'] == 'Mstra') {
                $textfirstname = 'firstnamec'.$j;
                $textlastname = 'lastnamec'.$j;
                $texttitle = 'titlec'.$j;
                $valuetitle = 'Mstr';
                $textbirthday = 'birthdatec'.$j;
            } elseif ($wisatawan[$i]['title'] == 'Missa') {
                $textfirstname = 'firstnamec'.$j;
                $textlastname = 'lastnamec'.$j;
                $texttitle = 'titlec'.$j;
                $valuetitle = 'Miss';
                $textbirthday = 'birthdatec'.$j;
            } elseif ($wisatawan[$i]['title'] == 'Mstrb') {
                $textfirstname = 'firstnamei'.$j;
                $textlastname = 'lastnamei'.$j;
                $texttitle = 'titlei'.$j;
                $valuetitle = 'Mstr';
                $textbirthday = 'birthdatei'.$j;
            } elseif ($wisatawan[$i]['title'] == 'Missb') {
                $textfirstname = 'firstnamei'.$j;
                $textlastname = 'lastnamei'.$j;
                $texttitle = 'titlei'.$j;
                $valuetitle = 'Miss';
                $textbirthday = 'birthdatei'.$j;
            } else {
                $textfirstname = 'firstnamea'.$j;
                $textlastname = 'lastnamea'.$j;
                $texttitle = 'titlea'.$j;
                $valuetitle = $wisatawan[$i]['title'];
                $textbirthday = 'birthdatea'.$j;
            }

            $wisatawan = [
                $textfirstname => $wisatawan[$i]['namaDepan'],
                $textlastname => $wisatawan[$i]['namaBelakang'],
                $texttitle => $valuetitle,
                $textbirthday => date("Y-m-d", strtotime($wisatawan[$i]['tglLahir']))
            ];

            $datawisatawan=array_merge($datawisatawan,$wisatawan);
        }

        $variable = [
            'token'=>$token,
            'adult'=>$parameter['dewasa'],
            'child'=>$parameter['anak'],
            'infant'=>$parameter['batita'],
            'flight_id'=>$request->input('idPergi'),
            'ret_flight_id'=>$request->input('idPulang'),
            'conSalutation'=>$user->title,
            'conFirstName'=>$user->nama_depan,
            'conLastName'=>$user->nama_belakang,
            'conPhone'=>$user->no_telpon,
            'conEmailAddress'=>$user->email,
            'output'=>'json'
        ];

        $variable=array_merge($variable,$datawisatawan);

        $order = \Curl::to('https://api-sandbox.tiket.com/order/add/flight')
            ->withData($variable)
            ->get();

        return json_decode($order, true);
    }

    public static function getHotel(Request $request)
    {
        ApiController::tokenHotel($request);

        $get = \Curl::to('https://api-sandbox.tiket.com/search/hotel')
            ->withData([
                'token' => $request->session()->get('token.pesawat'),
                'q' => $request->input('tujuan'),
                'night' => $request->input('durasi'),
                'startdate' => $request->input('start'),
                'enddate' => $request->input('end'),
                'latitude' => $request->input('lat'),
                'longitude' => $request->input('lng'),
                'adult' => $request->input('dewasa'),
                'child' => $request->input('anak'),
                'room' => ceil(($request->input('dewasa') + $request->input('anak')) / 2),
                'maxprice' => $request->input('dana'),
                // 'sort' => true,
                'output' => 'json'
            ])
            ->get();
        
        return json_decode($get, true);
    }

    public static function detailHotel($url)
    {
        $get = \Curl::to($url)
            ->get();

        return json_decode($get, true);
    }

    public static function orderHotel(Request $request, $url)
    {
        $order = ApiController::cekOrder($request, 'hotel');

        if (isset($order['myorder']['data'])) {
            for ($i = 0;$i < count($order['myorder']['data']); $i++) {
                if ($order['myorder']['data'][$i]['order_type'] == 'hotel') {
                    ApiController::deleteOrder($order['myorder']['data'][$i]['delete_uri']);
                }
            }
        }

        $order = \Curl::to($url)
            ->get();

        return json_decode($order, true);
    }
}
