<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ObjekWisata;
use App\Models\TipeObjekWisata;
use App\Models\DetailTipeObjekWisata;
use App\Models\GambarObjekWisata;
use App\Models\Area;
use App\Models\Mobil;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.index');
    }

    public function objekWisata(Request $request)
    {
        $objekWisatas = ObjekWisata::with(['area', 'images', 'detailTipe.tipe'])->get();

        return view('admin.objekwisata.index', compact('objekWisatas'));
    }

    public function tambahObjekWisata(Request $request)
    {
        $areas = Area::all();
        $tipes = TipeObjekWisata::all();
        $days = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'];

        return view('admin.objekwisata.tambah', compact('areas', 'tipes', 'days'));
    }

    public function storeObjekWisata(Request $request)
    {
        try {
            DB::beginTransaction();

            $gambars = $request->file('gambar');
            $tipes = $request->input('tipe');
            $koordinat = $request->input('koordinat');
            $koordinat = explode(',', str_replace(' ', '', $koordinat));

            $objekWisata = new ObjekWisata();
            $objekWisata->id_area = $request->input('daerah');
            $objekWisata->nama = $request->input('nama');
            $objekWisata->deskripsi = $request->input('deskripsi');
            $objekWisata->alamat = $request->input('alamat');
            $objekWisata->harga_dewasa = $request->input('hargaDewasa');
            $objekWisata->harga_anak = $request->input('hargaAnak');
            $objekWisata->lat = $koordinat[0];
            $objekWisata->lng = $koordinat[1];
            $objekWisata->hari_buka = $request->input('hariBuka');
            $objekWisata->jam_buka = $request->input('jamBuka');
            $objekWisata->hari_tutup = $request->input('hariTutup');
            $objekWisata->jam_tutup = $request->input('jamTutup');
            $objekWisata->save();

            foreach ($tipes as $tipe) {
                DetailTipeObjekWisata::create([
                    'id_objek_wisata' => $objekWisata->id,
                    'id_tipe_objek_wisata' => $tipe
                ]);
            }

            foreach ($gambars as $gambar) {
                $path = $gambar->store('images/objek-wisata', 'public_upload');

                GambarObjekWisata::create([
                    'id_objek_wisata' => $objekWisata->id,
                    'path' => $path
                ]);
            }

            DB::commit();
            return response('', 200);
        } catch (\Exception $ex) {
            DB::rollBack();
            dd($ex);
            return response()->json($ex->getMessage(), 500);
        }
    }

    public function mobil(Request $request)
    {
        $mobils = Mobil::all();

        return view('admin.mobil.index', compact('mobils'));
    }

    public function tambahMobil(Request $request)
    {
        return view('admin.mobil.tambah');
    }

    public function storeMobil(Request $request)
    {
        $gambar = $request->file('gambar');
        $path = $gambar->store('images/mobil', 'public_upload');

        Mobil::create([
            'merk' => $request->input('merk'),
            'model' => $request->input('model'),
            'jenis' => $request->input('jenis'),
            'jumlah_penumpang' => $request->input('penumpang'),
            'harga_supir_bensin' => $request->input('sopir'),
            'harga_kosongan' => $request->input('hari'),
            'image_path' => $path
        ]);

        return response()->json('', 200);
    }
}
