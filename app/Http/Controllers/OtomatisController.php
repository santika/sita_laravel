<?php

namespace App\Http\Controllers;

header("Content-Type: text/event-stream\n\n");
header("Cache-Control: no-cache");
header("Connection: keep-alive");

use Illuminate\Http\Request;
use App\Models\ObjekWisata;
use App\Models\Mobil;
use Carbon\Carbon;

class OtomatisController extends Controller
{
    public function index(Request $request)
    {
        if ($request->session()->get('parameter.type') === 'manual') {
            return response('', 404);
        }

        if (!$request->session()->has('order.pesawat') && !$request->session()->has('biaya.pesawat')) {
            $pesawat = $this->pesawat($request);

            if (!$pesawat) {
                $this->send('1', 'Proses Memesan Penerbangan Gagal.');
                $request->session()->flush();
                die();
            }

            $this->send('1');
            sleep(1);
        } else {
            $this->send('1');
            sleep(1);
        }

        if (!$request->session()->has('order.hotel') && !$request->session()->has('biaya.hotel')) {
            $hotel = $this->hotel($request);

            if (!$hotel) {
                $this->send('2', 'Proses Memesan Hotel Gagal.');
                $request->session()->flush();
                die();
            }

            $this->send('2');
            sleep(1);
        } else {
            $this->send('2');
            sleep(1);
        }

        if (!$request->session()->has('objekWisata') && !$request->session()->has('objekWisata')) {
            $objekWisata = $this->objekWisata($request);

            if (!$objekWisata) {
                $this->send('3', 'Proses Memesan Objek Wisata Gagal.');
                $request->session()->flush();
                die();
            }

            $this->send('3');
            sleep(1);
        } else {
            $this->send('3');
            sleep(1);
        }

        if ($request->session()->get('parameter.mobil') != 0) {
            if ($request->session()->get('parameter.mobil') === 1) {
                $mobil = $this->mobil($request);

                if (!$mobil) {
                    $this->send('4', 'Proses Memesan Mobil Gagal.');
                    $request->session()->flush();
                    die();
                }
            } elseif ($request->session()->get('parameter.mobil') === 2) {
                $mobil = $this->mobil($request, true);

                if (!$mobil) {
                    $this->send('4', 'Proses Memesan Mobil Gagal.');
                    $request->session()->flush();
                    die();
                }
            }

            $this->send('4');
            sleep(1);
        } else {
            $this->send('4');
            sleep(1);
        }
        // dump($request->session()->all());
    }

    private function send($proses, $error = null)
    {
        echo "event:message\n";
        echo "error:$error\n";
        echo "data:$proses\n\n";
        ob_flush();
        flush();
    }

    public function clear(Request $request)
    {
        $request->session()->forget('biaya');
        $request->session()->forget('order');
        $request->session()->forget('waktu');
    }

    private function pesawat(Request $request)
    {
        $pergi = Api\Tiket::pesawatAwal($request);
        $kembali = Api\Tiket::pesawatKembali($request);

        if (
            !is_null($pergi)
            && !is_null($kembali)
            && array_key_exists('departures', $pergi)
            && array_key_exists('result', $pergi['departures']) 
            && isset($pergi['departures']['result'])
            && array_key_exists('departures', $kembali)
            && array_key_exists('result', $kembali['departures']) 
            && isset($kembali['departures']['result'])
        
        ) {
            // cari biaya termurah
            $result = $pergi['departures']['result'];
            $prices = array_column($result, 'price_value');
            $minPergi = $result[array_search(min($prices), $prices)];
            $result = $kembali['departures']['result'];
            $prices = array_column($result, 'price_value');
            $minPulang = $result[array_search(min($prices), $prices)];

            // order penerbangan
            Api\Tiket::orderFlight($request, $minPergi['flight_id'], $minPulang['flight_id']);

            // simpan orderan
            $cek = Api\Tiket::cekOrder($request, 'pesawat');
            if ($cek['diagnostic']['status'] !== 200 || !array_key_exists('myorder', $cek)) {
                return false;
            }

            $dana = $request->session()->get('parameter.dana');
            $biaya = $cek['myorder']['total'];

            $request->session()->put('biaya.pesawat', $biaya);
            $request->session()->put('order.pesawat', $cek);

            $waktu['berangkat']['tgl']=$cek['myorder']['data'][0]['detail']['flight_date'];
            $waktu['berangkat']['jamBerangkat']=$cek['myorder']['data'][0]['detail']['departure_time'];
            $waktu['berangkat']['jamSampai']=$cek['myorder']['data'][0]['detail']['arrival_time'];
            $waktu['kembali']['tgl']=$cek['myorder']['data'][1]['detail']['flight_date'];
            $waktu['kembali']['jamBerangkat']=$cek['myorder']['data'][1]['detail']['departure_time'];
            $waktu['kembali']['jamSampai']=$cek['myorder']['data'][1]['detail']['arrival_time'];

            $request->session()->put('waktu', $waktu);

            return true;
        }

        return false;
    }

    private function hotel(Request $request)
    {
        $hotel = Api\Tiket::getHotel($request, true);

        if (
            !is_null($hotel)
            && array_key_exists('results', $hotel)
            && array_key_exists('result', $hotel['results'])
            && isset($hotel['results']['result'])
        ) {
            for ($i = 0; $i < count($hotel['results']['result']); $i++) { 
                // ambil data detail hotel
                $dataDetailHotel = Api\Tiket::detailHotel($hotel['results']['result'][$i]['business_uri'].'&token='.$request->session()->get('token.hotel').'&output=json');

                if (
                    $dataDetailHotel['diagnostic']['status'] != 200
                    || !array_key_exists('results', $dataDetailHotel) && !array_key_exists('result', $dataDetailHotel['results']) && count($dataDetailHotel['results']['result']) < 1
                    || !array_key_exists('general', $dataDetailHotel) && !array_key_exists('latitude', $dataDetailHotel['general']) && !array_key_exists('longitude', $dataDetailHotel['general'])
                ) {
                    continue;
                }

                $latlng = ['lat' => $dataDetailHotel['general']['latitude'], 'lng' => $dataDetailHotel['general']['longitude']];
                $request->session()->put('koordinat.hotel', $latlng);

                $start = $request->session()->get('waktu.berangkat.tgl').' '.$request->session()->get('waktu.berangkat.jamSampai');
                $end = $request->session()->get('waktu.kembali.tgl'). ' '.$request->session()->get('waktu.kembali.jamBerangkat');
                $cariDurasi = Api\Map::getDurationAndDistance($request, 'airport', $latlng['lat'].','.$latlng['lng'], strtotime($start));

                $end = Carbon::parse($end);
                $lamaTravel = Carbon::parse($start);
                $durasi = $cariDurasi['routes'][0]['legs'][0]['duration_in_traffic']['value'] + 7200;
                $lamaTravel->addSeconds($durasi);

                if ($lamaTravel->diffInHours($end, false) <= 5) {
                    continue;
                }

                $order = Api\Tiket::orderHotel($request, $dataDetailHotel['results']['result'][0]['bookUri'].'&token='.$request->session()->get('token.hotel').'&output=json');
                $cek = Api\Tiket::cekOrder($request, 'hotel');

                if ($order['diagnostic']['status'] == 200) {
                    $request->session()->put('order.hotel', $cek);
                    $request->session()->put('durasi.hotel', $durasi);
                    $request->session()->put('biaya.hotel', intval(number_format($cek['myorder']['data'][0]['subtotal_and_charge'], 0, '', '')));
                    $request->session()->forget('urlDetailHotel');

                    break;
                }
            }

            return true;
        }

        return false;
    }

    private function objekWisata(Request $request)
    {
        $area = $request->session()->get('parameter.tujuan');
        $tipe = $request->session()->get('parameter.tipe');
        $selected = $request->session()->get('objekWisata');

        try {
            $objekWisatas = ObjekWisata::with(['area', 'images', 'detailTipe.tipe'])
                ->whereHas('area', function ($query) use ($area) {
                    $query->where('area', $area);
                })
                ->whereHas('detailTipe', function ($query) use ($tipe) {
                    $query->where('id_tipe_objek_wisata', $tipe);
                })
                ->where(function ($query) use ($selected) {
                    if (!is_null($selected)) {
                        $query->whereNotIn('id', $selected);
                    }
                })
                ->get();

            foreach ($objekWisatas as $objekwisata) {
                $dana = str_replace('.', '', $request->session()->get('parameter.dana'));
                $pesawat = intval($request->session()->get('biaya.pesawat'));
                $objekWisata = intval($request->session()->get('biaya.objekWisata'));
                $mobil = intval($request->session()->get('biaya.mobil'));
                $hotel = intval($request->session()->get('biaya.hotel'));
                $sisaDana = ((($dana - $pesawat) - $hotel) - $objekWisata) - $mobil;
                $biaya = (intval($request->session()->get('parameter.dewasa')) * $objekwisata->harga_dewasa) + (intval($request->session()->get('parameter.anak')) * $objekwisata->harga_anak);

                if ($sisaDana > $biaya) {
                    $koordinat['latlng'] = $objekwisata->lat.','.$objekwisata->lng;
                    $start = $request->session()->get('waktu.berangkat.tgl').' '.$request->session()->get('waktu.berangkat.jamSampai');
                    $hotel = $request->session()->get('koordinat.hotel');

                    $cariDurasi = Api\Map::getDurationAndDistance($request, $hotel['lat'].','.$hotel['lng'], $request->input('latlng'), strtotime($start));

                    $durasi = intval($request->session()->get('durasi.hotel'));

                    if ($request->session()->has('durasi.objekWisata')) {
                        $durasiObjekwisata = $request->session()->get('durasi.objekWisata');

                        foreach ($durasiObjekwisata as $objekWisata) {
                            $durasi = $durasi + $objekWisata;
                        }
                    }

                    $start = Carbon::parse($start);
                    $end = Carbon::parse($request->session()->get('waktu.kembali.tgl').' '.$request->session()->get('waktu.kembali.jamBerangkat'));
                    $start->addSeconds($durasi);

                    if ($start->diffInHours($end, false) <= 5) {
                        continue;
                    }

                    $request->session()->push('objekWisata', $objekwisata->id);
                    $request->session()->push('biaya.objekWisata', $biaya);
                    $request->session()->push('koordinat.objekWisata', $koordinat);

                    continue;
                } else {
                    continue;
                }
            }

            return true;
        } catch (\Exception $exception) {
            dump($exception);
            return false;
        };

        return false;
    }

    private function mobil(Request $request, $supir = false)
    {
        $dana = str_replace('.', '', $request->session()->get('parameter.dana'));
        $pesawat = intval($request->session()->get('biaya.pesawat'));
        $objekWisata = intval($request->session()->get('biaya.objekWisata'));
        $hotel = intval($request->session()->get('biaya.hotel'));
        $sisaDana = (($dana - $pesawat) - $hotel) - $objekWisata;

        $selected = $request->session()->get('mobil');

        if ($supir) {
            $sortBy = 'harga_supir_bensin';
        } else {
            $sortBy = 'harga_kosongan';
        }

        $mobil = Mobil::where('harga_kosongan', '<', $sisaDana)
            ->where(function ($query) use ($selected) {
                if (!is_null($selected)) {
                    $query->whereNotIn('id', $selected);
                }
            })
            ->orderBy($sortBy, 'asc')
            ->first();

        if (is_null($mobil)) {
            return false;
        }

        if ($supir) {
            $request->session()->put('mobil.id', $mobil->id);
            $request->session()->put('mobil.supir', $supir);
            $request->session()->put('biaya.mobil', $mobil->harga_supir_bensin);
        } else {
            $request->session()->put('mobil.id', $mobil->id);
            $request->session()->put('mobil.supir', $supir);
            $request->session()->put('biaya.mobil', $mobil->harga_supir_bensin);
        }

        return true;
    }
}
