<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi;

class TransaksiPesawat extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_transaksi',
        'order_name',
        'order_name_detail',
        'logo',
        'harga',
        'harga_plus_pajak',
        'tgl_berangkat',
        'jam_berangkat',
        'jam_sampai',
        'pulang_pergi',
    ];

    public function transaksi()
    {
        return $this->belongsTo(Transaksi::class, 'id_transaksi');
    }
}
