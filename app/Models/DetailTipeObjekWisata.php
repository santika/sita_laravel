<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailTipeObjekWisata extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_objek_wisata',
        'id_tipe_objek_wisata',
    ];

    public function objekWisata()
    {
        return $this->belongsTo(ObjekWisata::class, 'id_objek_wisata');
    }

    public function tipe()
    {
        return $this->belongsTo(TipeObjekWisata::class, 'id_tipe_objek_wisata');
    }
}
