<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Area;

class Transaksi extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user',
        'id_area',
        'tgl_transaksi',
        'asal',
        'dana',
        'start',
        'end',
        'dewasa',
        'anak',
        'batita',
        'order_id_pesawat',
        'order_id_hotel',
        'token_pesawat',
        'token_hotel',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function area()
    {
        return $this->belongsTo(Area::class, 'id_area');
    }

    public function hotel()
    {
        return $this->hasMany(TransaksiHotel::class, 'id_transaksi');
    }

    public function pesawat()
    {
        return $this->hasMany(TransaksiPesawat::class, 'id_transaksi');
    }
}
