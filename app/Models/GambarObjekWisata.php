<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GambarObjekWisata extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_objek_wisata',
        'path',
    ];

    public function objekWisata()
    {
        return $this->belongsTo(ObjekWisata::class, 'id_objek_wisata');
    }
}
