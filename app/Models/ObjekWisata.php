<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ObjekWisata extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama',
        'deskripsi',
        'alamat',
        'harga_dewasa',
        'harga_anak',
        'lat',
        'lng',
        'hari_buka',
        'jam_buka',
        'hari_tutup',
        'jam_tutup',
    ];

    public function area()
    {
        return $this->belongsTo(Area::class, 'id_area');
    }

    public function detailTipe()
    {
        return $this->hasMany(DetailTipeObjekWisata::class, 'id_objek_wisata');
    }

    public function images()
    {
        return $this->hasMany(GambarObjekWisata::class, 'id_objek_wisata');
    }
}
