<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipeObjekWisata extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_tipe',
    ];

    public function detailTipe()
    {
        $this->hasMany(DetailTipeObjekwisata::class);
    }
}
