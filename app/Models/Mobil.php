<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mobil extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'merk',
        'model',
        'jenis',
        'jumlah_penumpang',
        'harga_supir_bensin',
        'harga_kosongan',
        'image_path',
    ];
}
