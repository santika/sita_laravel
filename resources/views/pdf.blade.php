<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
            body {
                font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
                fondt-size: 14px;
            }
            .container {
                min-width: 600px;
            }
            .table {
                display: table;
                width: 100%;
                border: 1px solid rgba(34,36,38,0.5);
                border-radius: 0.28571429em;
                color: rgba(0, 0, 0, 0.87);
                padding: 0;
            }
            table {
                margin: 0;
                width: 100%;
                border-collapse: collapse;
            }
            thead tr:nth-child(1) {
                background-color: #dcdcdc;
            }
            th, td {
                padding: 10px;
                text-transform: capitalize;
                border-bottom: 1px solid #696969;
            }
            th:nth-child(1), td:nth-child(1) {
                border-right: 1px solid #696969;
                width: 153px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="table">
                <table>
                    <thead>
                        <tr>
                            <th>Waktu</th>
                            <th>Kegiatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($jadwals as $jadwal)
                            <tr>
                                <td>
                                    @foreach ($jadwal->transaksi->pesawat as $pesawat)
                                        @if ($loop->first)
                                            {{ date("d M Y", strtotime($pesawat->tgl_berangkat)) }} ({{ date("H:i", strtotime($pesawat->jam_berangkat)) }})
                                        @endif
                                    @endforeach
                                </td>
                                <td>berangkat ke bali</td>
                            </tr>
                            @php
                                $schedule = json_decode($jadwal->jadwal, true)
                            @endphp
                            @for ($i = 0; $i < count($schedule['schedule']); $i++)
                                @php
                                    if ($i == 0) {
                                        $textTitleOTW = 'tiba di bali & melanjutkan perjalanan ke ';
                                        $textTitleTibaDiTujuan = '';
                                        $iconOTW = '<i class="vertically flipped big plane icon"></i>';
                                    } else {
                                        $textTitleOTW = 'melanjutkan perjalanan ke ';
                                        $textTitleTibaDiTujuan = 'tiba di ';
                                        $iconOTW = '<i class="big car icon"></i>';
                                    }

                                    foreach ($jadwal->transaksi->hotel as $hotel) {
                                        if (array_key_exists(0, $schedule['schedule'][$i]) && $schedule['schedule'][$i]['tipe'] === 'hotel') {
                                            $titleOTW = $textTitleOTW.$hotel->order_name;
                                            $titleTibaDiTujuan = $hotel->order_name;
                                            $durasiPerjalanan = $schedule['schedule'][$i][0]['duration']['text'];
                                            $jarakPerjalanan = $schedule['schedule'][$i][0]['distance']['text'];
                                            $textInfo = '<i class="big hotel icon"></i>'.$hotel->order_name_detail;
                                            $iconKiri = '<i class="big hotel icon"></i>';
                                        } elseif ($schedule['schedule'][$i]['tipe']=='objekwisata') {
                                            $titleOTW = $textTitleOTW.$schedule['schedule'][$i]['objekwisata']['objek_wisata']['nama'];
                                            $titleTibaDiTujuan = $schedule['schedule'][$i]['objekwisata']['objek_wisata']['nama'];
                                            $durasiPerjalanan = $schedule['schedule'][$i]['distanceDuration']['duration']['text'];
                                            $jarakPerjalanan = $schedule['schedule'][$i]['distanceDuration']['distance']['text'];
                                            $textInfo = '<i class="marker icon"></i> '.$schedule['schedule'][$i]['objekwisata']['objek_wisata']['alamat'];
                                            $iconKiri = '<i class="big tree icon"></i>';
                                        } elseif (array_key_exists(0, $schedule['schedule'][$i]) && $schedule['schedule'][$i]['tipe'] === 'last') {
                                            $titleOTW = 'perjalanan ke bandara untuk pulang';
                                            $titleTibaDiTujuan = 'kembali ke kota asal';
                                            $durasiPerjalanan = $schedule['schedule'][$i][0]['duration']['text'];
                                            $jarakPerjalanan = $schedule['schedule'][$i][0]['distance']['text'];
                                            $textInfo = '<i class="big building icon"></i>'.$pesawat->order_name_detail.' - '.$pesawat->order_name;
                                            $iconKiri = '<i class="big building icon"></i>';
                                        }
                                    }
                                @endphp
                                @if ($i === 0)
                                    <tr>
                                        <td>
                                            @php
                                                $time = new DateTime($schedule['schedule'][$i]['time']['go']);
                                            @endphp
                                            {{ $time->format('d M Y') }} ({{ $time->format('H:i') }})
                                        </td>
                                        <td>{{ $titleOTW }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <td>
                                        @php
                                            $time = new DateTime($schedule['schedule'][$i]['time']['arrive']);
                                        @endphp
                                            {{ $time->format('d M Y') }} ({{ $time->format('H:i') }})
                                    </td>
                                    <td>{{ $titleTibaDiTujuan }}</td>
                                </tr>
                            @endfor
                            @foreach ($jadwal->transaksi->pesawat as $pesawat)
                                @if ($loop->last)
                                    <tr>
                                        <td>{{ date("d M Y", strtotime($pesawat->tgl_berangkat)) }} ({{ date("H:i", strtotime($pesawat->jam_berangkat)) }})</td>
                                        <td>berangkat pulang</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ date("d M Y", strtotime($pesawat->tgl_berangkat)) }} ({{ date("H:i", strtotime($pesawat->jam_sampai)) }})
                                        </td>
                                        <td>tiba di kota asal</td>
                                    </tr>
                                @endif
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>