<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SITA</title>
    <link rel="stylesheet" href="{{ asset('css/css.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
    <link rel="stylesheet" href="https://unpkg.com/jquery-form-validator@^2/src/theme-default.css">
    <link rel="stylesheet" href="https://unpkg.com/jquery-datetimepicker@^2/build/jquery.datetimepicker.min.css">
    <link rel="stylesheet" href="https://unpkg.com/izitoast@^1/dist/css/iziToast.min.css">
    @yield('style')
</head>
<body @if(url()->current() == url('/')) @if (Auth::user()) {{ "onload=login(true)" }} @else {{ "onload=login(false)" }} @endif @endif>
    <div class="ui fluid fixed blue inverted menu">
        @if (Auth::check() && Auth::user()->role == 'Admin')
            <a class="sidebar item">
                <i class="sidebar icon"></i>
            </a>
        @endif
        <div class="ui container">
            <div class="right menu">
                <a href="{{ route('landingpage.index') }}" class="item">
                    <i class="home icon"></i>
                </a>
                @auth
                    <!-- <a href="<?php //echo site_url('user'); ?>" class="item">
                        <i class="user icon"></i>
                    </a> -->
                    <a class="item" href="{{ route('logout') }}">
                        <i class="ui sign out icon"></i>
                    </a>
                @endauth
                @guest
                    <a class="item" href="{{ route('register') }}">
                        Daftar
                    </a>
                    <a class="item login">
                        Masuk
                    </a>
                @endguest
            </div>
        </div>
    </div>
    @yield('content')
    <div class="ui page dimmer">
        <div class="ui text loader">Mohon Tunggu</div>
    </div>
</body>
<script src="https://unpkg.com/jquery@^3/dist/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/izitoast@^1/dist/js/iziToast.min.js"></script>
<script src="https://unpkg.com/jquery-datetimepicker@^2/build/jquery.datetimepicker.full.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="https://unpkg.com/jquery-form-validator@^2/form-validator/jquery.form-validator.min.js"></script>
<script src="https://unpkg.com/accounting-js@1.1.1/dist/accounting.umd.js"></script>
<script>
    $('.ui.page.dimmer').dimmer({
        closable: false
    });
</script>
@yield('script')
</html>