@extends('layouts.home')

@section('content')
<div class="ui sidebar inverted vertical labeled icon menu">
    <a class="item">&nbsp</a>
    <a class="item" href="{{ route('admin.objekwisata') }}">
        <i class="tree icon"></i>
        Objek Wisata
    </a>
    <a class="item" href="{{ route('admin.mobil') }}">
        <i class="car icon"></i>
        Mobil
    </a>
</div>
<div class="ui container content">
    <div class="top divider"></div>
    <div class="top divider"></div>
    <div class="ui grid">
        <div class="sixteen wide column">
            <div class="ui segments">
                <div class="ui clearing segment">
                    <a class="ui right floated green button" href="{{ route('admin.mobil.tambah') }}">Tambah</a>
                </div>
                @foreach ($mobils as $mobil)
                    <div class="ui clearing segment">
                        <div class="ui hidden divider"></div>
                        <div class="ui small image" style="float:left;">
                            <img src="{{ asset($mobil->image_path) }}" alt="Gambar {{ $mobil->model }}">
                        </div>
                        <div class="content" style="padding-left:160px;">
                            <h3 class="ui header">
                            {{ $mobil->model }}
                            <div class="ui sub header">
                                <i class="user icon"></i>
                                {{ $mobil->jumlah_penumpang }} Orang
                            </div>
                            </h3>
                            <div class="ui two column relaxed grid">
                                <div class="ui center aligned column">
                                    <b>Tanpa Supir: {{ 'Rp '.number_format($mobil->harga_kosongan, 0, '.', ',') }}</b>
                                </div>
                                <div class="ui vertical divider" style="left:50%;height:calc(30% - 1rem);transform:translateX(50%);">Atau</div>
                                <div class="ui center aligned column">
                                    <b>Dengan supir + Bensin: {{ 'Rp '.number_format($mobil->harga_supir_bensin, 0, '.', ',') }}</b>
                                </div>
                            </div>
                        </div>
                        <div class="ui hidden divider"></div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="ui bottom attached footer"></div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $('.sidebar.item').click(function() {
        $('.ui.sidebar').sidebar({
            onHidden:function() {
            $('body').removeClass('pushable');
            }
        }).sidebar('toggle');
    });
</script>
@endsection