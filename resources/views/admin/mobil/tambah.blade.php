@extends('layouts.home')

@section('content')
<div class="ui sidebar inverted vertical labeled icon menu">
    <a class="item">&nbsp</a>
    <a class="item" href="{{ route('admin.objekwisata') }}">
        <i class="tree icon"></i>
        Objek Wisata
    </a>
    <a class="item" href="{{ route('admin.mobil') }}">
        <i class="car icon"></i>
        Mobil
    </a>
</div>
<div class="ui container content">
    <div class="top divider"></div>
    <div class="top divider"></div>
    <div class="ui grid">
        <div class="sixteen wide column">
            <div class="ui segments">
                <div class="ui segment">
                    <form class="ui form tambah mobil">
                        <div class="fields">
                            <div class="three wide field">
                                <label for="merk">Merk</label>
                                <input type="text" name="merk" id="merk">
                            </div>
                            <div class="five wide field">
                                <label for="model">Model</label>
                                <input type="text" name="model" id="model">
                            </div>
                            <div class="four wide field">
                                <label for="jenis">Jenis</label>
                                <select name="jenis" id="jenis" class="ui dropdown">
                                    <option value="">Jenis Mobil</option>
                                    <option value="Sedan">Sedan</option>
                                    <option value="Minibus">Minibus</option>
                                    <option value="Minivan">Minivan</option>
                                </select>
                            </div>
                            <div class="four wide field">
                                <label for="penumpang">Penumpang</label>
                                <select class="ui search selection dropdown" name="penumpang">
                                    <option value="">Jumlah Penumpang</option>
                                    @for ($i = 4; $i < 20; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field">
                                <label for="hari">Harga Per Hari</label>
                                <input type="text" name="hari" id="hari">
                            </div>
                            <div class="field">
                                <label for="sopir">Harga Per Hari + Sopir + Bensin</label>
                                <input type="text" name="sopir" id="sopir">
                            </div>
                        </div>
                        <div class="field">
                            <label>Gambar Mobil</label>
                        </div>
                        <div class="field gambar">
                            <input type="file" name="gambar" id="gambar">
                        </div>
                        <div class="field button">
                            <div class="ui mobil submit fluid green button">Submit</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('.sidebar.item').click(function() {
        $('.ui.sidebar').sidebar({
            onHidden:function() {
            $('body').removeClass('pushable');
            }
        }).sidebar('toggle');
    });

    $('.ui.dropdown').dropdown();

    $('.mobil.submit.button').on('click', function () {
        let data = new FormData($('.form.tambah.mobil')[0]);
        $('.ui.page.dimmer').dimmer('show');

        axios.post("{{ route('admin.mobil.store') }}", data, {
            headers: {
                'processData': false,
                'contentType': false,
            }
        }).then((response) => {
            // $('.ui.page.dimmer').dimmer('hide');
            location.reload(true);
        }).catch((error) => {
            $('.ui.page.dimmer').dimmer('hide');

            iziToast.error({
                title: 'Action Failed',
                message: error.response.statusText
            });
        })
    });
</script>
@endsection