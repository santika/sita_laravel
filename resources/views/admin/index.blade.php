@extends('layouts.home')

@section('content')
<div class="ui sidebar inverted vertical labeled icon menu">
    <a class="item">&nbsp</a>
    <a class="item" href="{{ route('admin.objekwisata') }}">
        <i class="tree icon"></i>
        Objek Wisata
    </a>
    <a class="item" href="{{ route('admin.mobil') }}">
        <i class="car icon"></i>
        Mobil
    </a>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $('.sidebar.item').click(function() {
        $('.ui.sidebar').sidebar({
            onHidden:function() {
            $('body').removeClass('pushable');
            }
        }).sidebar('toggle');
    });
</script>
@endsection