@extends('layouts.home')

@section('content')
<div class="ui sidebar inverted vertical labeled icon menu">
    <a class="item">&nbsp</a>
    <a class="item" href="{{ route('admin.objekwisata') }}">
        <i class="tree icon"></i>
        Objek Wisata
    </a>
    <a class="item" href="{{ route('admin.mobil') }}">
        <i class="car icon"></i>
        Mobil
    </a>
</div>
<div class="ui container content">
    <div class="top divider"></div>
    <div class="top divider"></div>
    <div class="ui grid">
        <div class="sixteen wide column">
        <div class="ui segments">
            <div class="ui segment">
            <div class="ui divided items">
                <div class="item">
                    <div class="content">
                        <a href="{{ route('admin.objekwisata.tambah') }}" class="ui right floated yellow button">Tambah</a>
                    </div>
                </div>
                @foreach ($objekWisatas as $objekwisata)
                    <div class="item">
                        <div class="ui small image">
                            @foreach ($objekwisata->images as $image)
                                @if ($loop->first)
                                    <img src="{{ asset($image->path) }}" alt="{{ $objekwisata->nama }}">
                                    @break
                                @endif
                            @endforeach
                        </div>
                        <div class="content">
                            <div class="header">
                                <a href="{{ route('admin.objekwisata.detail', [$objekwisata->id]) }}" style="color:#000">{{ $objekwisata->nama }}</a>
                            </div>
                            <div class="meta">
                                <a>Dewasa: {{ 'Rp ' . number_format($objekwisata->harga_dewasa, 0, '.', ',') }},</a>
                                <a>Anak-Anak: {{ 'Rp ' . number_format($objekwisata->harga_anak, 0, '.', ',') }},</a>
                                <a>{{ $objekwisata->alamat }}</a>
                            </div>
                            <div class="description">
                                <p style="text-align:justify">
                                    {{ str_limit(nl2br($objekwisata->deskripsi), 200) }} <a href="{{ route('admin.objekwisata.detail', [$objekwisata->id]) }}">[Lebih Lengkap]</a>
                                </p>
                            </div>
                            <div class="extra">
                                <div class="ui label">
                                    {{ $objekwisata->hari_buka }} - {{ $objekwisata->jam_buka }}
                                </div>
                                <div class="ui label">
                                    {{ $objekwisata->hari_tutup }} - {{ $objekwisata->jam_tutup }}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
<div class="ui bottom attached footer"></div>
<div class="ui tiny lewat modal">
    <div class="header">
        Apakah Anda Yakin Akan Membatalkan Proses Pencarian Objek Wisata?
    </div>
    <div class="actions">
        <div class="ui red approve button">
            Yakin
        </div>
        <div class="ui blue cancel button">
            Batal
        </div>
    </div>
</div>
<div class="ui tiny waktu habis modal">
    <div class="header">
        Durasi Berlibur Anda Telah Habis
    </div>
    <div class="actions">
        <div class="ui blue approve button">
            Ok
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $('.sidebar.item').click(function() {
        $('.ui.sidebar').sidebar({
            onHidden:function() {
            $('body').removeClass('pushable');
            }
        }).sidebar('toggle');
    });
</script>
@endsection