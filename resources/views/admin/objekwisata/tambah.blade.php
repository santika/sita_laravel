@extends('layouts.home')

@section('content')
<div class="ui sidebar inverted vertical labeled icon menu">
    <a class="item">&nbsp</a>
    <a class="item" href="{{ route('admin.objekwisata') }}">
        <i class="tree icon"></i>
        Objek Wisata
    </a>
    <a class="item" href="{{ route('admin.mobil') }}">
        <i class="car icon"></i>
        Mobil
    </a>
</div>
<div class="ui container content">
    <div class="top divider"></div>
    <div class="top divider"></div>
    <div class="ui grid">
        <div class="sixteen wide column">
            <div class="ui segments">
                <div class="ui segment">
                    <form class="ui form tambah wisata">
                        <div class="field">
                            <label>Area</label>
                            <select class="ui dropdown" name="daerah">
                                <option value="">Daerah Objek Wisata</option>
                                @foreach ($areas as $area)
                                    <option value="{{ $area->id }}">{{ $area->area }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <label>Nama Objek Wisata</label>
                            <input type="text" name="nama" id="nama" placeholder="Nama Objek Wisata">
                        </div>
                        <div class="field">
                            <label>Deskripsi</label>
                            <textarea name="deskripsi" id="deskripsi" cols="30" rows="10" placeholder="Deskripsi"></textarea>
                        </div>
                        <div class="field">
                            <label>Alamat</label>
                            <input type="text" name="alamat" id="alamat" placeholder="Alamat">
                        </div>
                        <div class="two fields">
                            <div class="field">
                                <label>Harga Dewasa</label>
                                <input type="text" name="hargaDewasa" id="hargaDewasa" placeholder="Harga Dewasa">
                            </div>
                            <div class="field">
                                <label>Harga Anak-Anak</label>
                                <input type="text" name="hargaAnak" id="hargaAnak" placeholder="Harga Anak-Anak">
                            </div>
                        </div>
                        <div class="five fields">
                            <div class="field">
                                <label>Hari Buka</label>
                                <select name="hariBuka" id="hariBuka" class="ui compact dropdown">
                                    <option value="">Hari Buka</option>
                                    @foreach ($days as $day)
                                        <option value="{{ $day }}">{{ $day }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field">
                                <label>Jam Buka</label>
                                <input type="time" name="jamBuka" id="jamBuka" placeholder="Jam Buka">
                            </div>
                            <div class="field">
                                <label>Hari Tutup</label>
                                <select name="hariTutup" id="hariTutup" class="ui compact dropdown">
                                    <option value="">Hari Buka</option>
                                    @foreach ($days as $day)
                                        <option value="{{ $day }}">{{ $day }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field">
                                <label>Jam tutup</label>
                                <input type="time" name="jamTutup" id="jamTutup" placeholder="Jam Tutup">
                            </div>
                            <div class="field">
                                <label>Durasi</label>
                                <input type="text" name="durasi" id="durasi" placeholder="Durasi">
                            </div>
                        </div>
                        <div class="field">
                            <label>Koordinat</label>
                            <input type="text" name="koordinat" id="korrdinat" placeholder="Koordinat">
                        </div>
                        <div class="field">
                            <label>Tipe Objek Wisata</label>
                            <select class="ui dropdown" name="tipe[]" multiple>
                                <option value="">Tipe Objek Wisata</option>
                                @foreach ($tipes as $tipe)
                                    <option value="{{ $tipe->id }}">{{ $tipe->nama_tipe }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <label>Gambar Objek Wisata</label>
                        </div>
                        <div class="field gambar">
                            <div class="ui action input">
                                <input type="file" name="gambar[]" id="gambar">
                                <div class="ui blue add button">
                                    <i class="plus icon"></i>
                                </div>
                            </div>
                        </div>
                        <div class="field button">
                            <div class="ui yellow submit fluid button">Submit</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $('.sidebar.item').click(function() {
        $('.ui.sidebar').sidebar({
            onHidden:function() {
            $('body').removeClass('pushable');
            }
        }).sidebar('toggle');
    });

    $('.dropdown').dropdown();

    $(document).on('click', '.add', function (e) {
        var controlForm = $('form:first'),
            currentEntry = $(this).parents('.field.gambar:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm).insertBefore('.field.button');

        newEntry.find('input').val('');

        controlForm.find('.field.gambar:not(:last) .add')
            .removeClass('add').addClass('remove')
            .removeClass('blue').addClass('red')
            .html('<i class="minus icon"></i>');
    }).on('click', '.remove', function(e) {
        $(this).parents('.field.gambar:first').remove();
  	});

    $('.submit.button').on('click', function () {
        let data = new FormData($('.form.tambah.wisata')[0]);
        $('.ui.page.dimmer').dimmer('show');

        axios.post("{{ route('admin.objekwisata.store') }}", data, {
            headers: {
                'processData': false,
                'contentType': false,
            }
        }).then((response) => {
            // $('.ui.page.dimmer').dimmer('hide');
            location.reload(true);
        }).catch((error) => {
            $('.ui.page.dimmer').dimmer('hide');

            iziToast.error({
                title: 'Action Failed',
                message: error.response.statusText
            });
        })
    })
</script>
@endsection