@extends('layouts.home')

@section('content')
<div class="ui container content">
    <div class="top divider"></div>
    <div class="top divider"></div>
    <div class="ui grid">
        <div class="sixteen wide column">
            <div class="ui blue raised segment" id="segment">
                <form action="{{ route('home.tambahwisatawan') }}" class="ui form" id="form">
                    @for ($i = 0; $i < $parameters['dewasa']; $i++)
                        <div class="ui blue inverted attached segment">
                            <h3 class="ui header">Data Wisatawan Dewasa {{ $i+1 }}</h3>
                        </div>
                        <div class="ui attached segment">
                            <div class="four fields">
                                <div class="field">
                                    <label>Title</label>
                                    <select class="ui dropdown" name="title[]" data-validation="empty">
                                        <option value="">Title</option>
                                        <option value="Mr">Bapak</option>
                                        <option value="Mrs">Ibu</option>
                                        <option value="Ms">Nona</option>
                                    </select>
                                </div>
                                <div class="field">
                                    <label>Nama Depan</label>
                                    <input type="text" name="nd[]" id="nd" placeholder="Nama Depan" data-validation="empty">
                                </div>
                                <div class="field">
                                    <label>Nama Belakang</label>
                                    <input type="text" name="nb[]" id="nb" placeholder="Nama Belakang" data-validation="empty">
                                </div>
                                <div class="field">
                                    <label>Tanggal Lahir</label>
                                    <div class="ui calendar">
                                        <div class="ui left icon input">
                                            <i class="calendar icon"></i>
                                            <input type="text" name="tgl[]" id="tgl" placeholder="Tanggal Lahir" data-validation="empty">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endfor
                    @for ($i = 0; $i < $parameters['anak']; $i++)
                        <div class="ui blue inverted attached segment">
                            <h3 class="ui header">Data Wisatawan Anak-Anak {{ $i+1 }}</h3>
                        </div>
                        <div class="ui attached segment">
                            <div class="four fields">
                                <div class="field">
                                    <label>Title</label>
                                    <select class="ui dropdown" name="title[]" data-validation="empty">
                                        <option value="">Title</option>
                                        <option value="Mstra">Anak Laki-Laki</option>
                                        <option value="Missa">Anak Perempuan</option>
                                    </select>
                                </div>
                                <div class="field">
                                    <label>Nama Depan</label>
                                    <input type="text" name="nd[]" id="nd" placeholder="Nama Depan" data-validation="empty">
                                </div>
                                <div class="field">
                                    <label>Nama Belakang</label>
                                    <input type="text" name="nb[]" id="nb" placeholder="Nama Belakang" data-validation="empty">
                                </div>
                                <div class="field">
                                    <label>Tanggal Lahir</label>
                                    <div class="ui calendar">
                                        <div class="ui left icon input">
                                            <i class="calendar icon"></i>
                                            <input type="text" name="tgl[]" id="tgl" placeholder="Tanggal Lahir" data-validation="empty">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endfor
                    @for ($i = 0; $i < $parameters['batita']; $i++)
                        <div class="ui blue inverted attached segment">
                            <h3 class="ui header">Data Wisatawan 3 Tahun Kebawah {{ $i+1 }}</h3>
                        </div>
                        <div class="ui attached segment">
                            <div class="four fields">
                                <div class="field">
                                    <label>Title</label>
                                    <select class="ui dropdown" name="title[]" data-validation="empty">
                                        <option value="">Title</option>
                                        <option value="Mstrb">Batita Laki-Laki</option>
                                        <option value="Missb">Batita Perempuan</option>
                                    </select>
                                </div>
                                <div class="field">
                                    <label>Nama Depan</label>
                                    <input type="text" name="nd[]" id="nd" placeholder="Nama Depan" data-validation="empty">
                                </div>
                                <div class="field">
                                    <label>Nama Belakang</label>
                                    <input type="text" name="nb[]" id="nb" placeholder="Nama Belakang" data-validation="empty">
                                </div>
                                <div class="field">
                                    <label>Tanggal Lahir</label>
                                    <div class="ui calendar">
                                        <div class="ui left icon input">
                                            <i class="calendar icon"></i>
                                            <input type="text" name="tgl[]" id="tgl" placeholder="Tanggal Lahir" data-validation="empty">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endfor
                    <div class="ui attached segment">
                        <input type="hidden" name="jumlah" value="{{ $parameters['dewasa'] + $parameters['anak'] + $parameters['batita'] }}">
                        <div class="ui fluid yellow submit button">
                            Proses
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="ui bottom attached footer"></div>
@endsection

@section('script')
<script>
    $('.ui.dropdown').dropdown();
    $('[id=tgl]').datetimepicker({
        timepicker:false,
        format:'d-m-Y',
        style:'border-radius:.28571429rem'
    })
    .focus(function () {
        $(this).blur();
    });

    $('.submit.button').on('click', function () {
        $('.page.dimmer').dimmer('show');
        axios.post($('#form').attr('action'), $('#form').serialize())
            .then((response) => {
                location.replace("{{ route('cari.pesawat') }}");
            })
            .catch((error) => {
                $('.page.dimmer').dimmer('hide');
                iziToast.error({
                    title: 'Action Failed',
                    message: error.response.data.message
                });
            });
    });
</script>
@endsection