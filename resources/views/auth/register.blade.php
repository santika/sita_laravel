@extends('layouts.home')

@section('content')
<header>
    <div class="masthead">
        <div class="top divider home"></div>
        <div class="ui container">
            <div class="ui blue raised segment">
                <form action="{{ route('register') }}" method="post" class="ui form register" id="formRegister">
                    @csrf
                    <div class="field">
                        <label>Email</label>
                        <input type="text" name="email" id="email" placeholder="Email">
                    </div>
                    <div class="field">
                        <label>Password</label>
                        <input type="password" name="password" id="password" placeholder="Password">
                    </div>
                    <div class="field">
                        <label>Title</label>
                        <select class="ui dropdown" name="title">
                            <option value="">Title</option>
                            <option value="Mr">Bapak</option>
                            <option value="Mrs">Ibu</option>
                            <option value="Ms">Nona</option>
                        </select>
                    </div>
                    <div class="field">
                        <label>Nama Depan</label>
                        <input type="text" name="namaDepan" id="namaDepan" placeholder="Nama Depan">
                    </div>
                    <div class="field">
                        <label>Nama Belakang</label>
                        <input type="text" name="namaBelakang" id="namaBelakang" placeholder="Nama Belakang">
                    </div>
                    <div class="field">
                        <label>Jenis Kelamin</label>
                        <select name="jk" id="jk" class="ui dropdown">
                            <option value="">Jenis Kelamin</option>
                            <option value="laki-laki">Laki-Laki</option>
                            <option value="perempuan">Perempuan</option>
                        </select>
                    </div>
                    <div class="field">
                        <label>Tanggal Lahir</label>
                        <div class="ui calendar">
                            <div class="ui left icon input">
                                <i class="calendar icon"></i>
                                <input type="text" name="tglLahir" id="tglLahir" placeholder="Tanggal Lahir">
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label>Alamat</label>
                        <input type="text" name="alamat" id="alamat" placeholder="Alamat">
                    </div>
                    <div class="field">
                        <label>Nomor Telepon</label>
                        <input type="text" name="telpon" id="telpon" placeholder="Nomor Telepon">
                    </div>
                    <div class="field">
                        <input type="submit" value="Submit" class="ui yellow submit fluid button">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="ui bottom attached footer"></div>
</header>
@endsection

@section('script')
<script>
    $('.inverted.menu').hide();
    $('.ui.dropdown').dropdown();

    $('.ui.slider.checkbox').checkbox();
    $('.ui.dropdown').dropdown();
    $('.ui.form.register').form({
        inline:true,
        fields: {
            password: 'minLength[8]',
            email: ['email', 'empty'],
            namaDepan: 'empty',
            namaBelakang: 'empty',
            title: 'empty',
            jk: 'empty',
            tglLahir: 'empty',
            alamat: 'empty',
            telpon: ['empty', 'number']
        }
    });
    $('#tglLahir')
        .datetimepicker({
            timepicker:false,
            format:'Y-m-d',
            style:'border-radius:.28571429rem'
        })
        .focus(function (e) {
            $(this).blur();
        });
</script>
@endsection