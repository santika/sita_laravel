@extends('layouts.home')

@section('content')
<div class="top divider"></div>
<div class="ui container">
    <div class="box">
        <ul>
            @foreach ($jadwals as $jadwal)
                <li>
                    <span></span>
                    <div class="title">berangkat ke bali</div>
                    @foreach ($jadwal->transaksi->pesawat as $pesawat)
                        @if ($loop->index === 0)
                            <div class="info">
                                <i class="plane icon"></i>
                                {{ $pesawat->order_name_detail }} - {{ $pesawat->order_name }}
                            </div>
                            <div class="time">
                                <span>
                                    {{ date("d M Y", strtotime($pesawat->tgl_berangkat)) }}
                                    <br>
                                    {{ date("H:i", strtotime($pesawat->jam_berangkat)) }}
                                </span>
                            </div>
                            @break
                        @endif
                    @endforeach
                </li>
                @php
                    $schedule = json_decode($jadwal->jadwal, true);
                @endphp
                @for ($i = 0; $i < count($schedule['schedule']); $i++)
                    @php
                        if ($i == 0) {
                            $textTitleOTW = 'tiba di bali & melanjutkan perjalanan ke ';
                            $textTitleTibaDiTujuan = '';
                            $iconOTW = '<i class="vertically flipped big plane icon"></i>';
                        } else {
                            $textTitleOTW = 'melanjutkan perjalanan ke ';
                            $textTitleTibaDiTujuan = 'tiba di ';
                            $iconOTW = '<i class="big car icon"></i>';
                        }

                        foreach ($jadwal->transaksi->hotel as $hotel) {
                            if (array_key_exists(0, $schedule['schedule'][$i]) && $schedule['schedule'][$i]['tipe'] === 'hotel') {
                                $titleOTW = $textTitleOTW.$hotel->order_name;
                                $titleTibaDiTujuan = $hotel->order_name;
                                $durasiPerjalanan = $schedule['schedule'][$i][0]['duration']['text'];
                                $jarakPerjalanan = $schedule['schedule'][$i][0]['distance']['text'];
                                $textInfo = '<i class="big hotel icon"></i>'.$hotel->order_name_detail;
                                $iconKiri = '<i class="big hotel icon"></i>';
                            } elseif ($schedule['schedule'][$i]['tipe']=='objekwisata') {
                                $titleOTW = $textTitleOTW.$schedule['schedule'][$i]['objekwisata']['objek_wisata']['nama'];
                                $titleTibaDiTujuan = $schedule['schedule'][$i]['objekwisata']['objek_wisata']['nama'];
                                $durasiPerjalanan = $schedule['schedule'][$i]['distanceDuration']['duration']['text'];
                                $jarakPerjalanan = $schedule['schedule'][$i]['distanceDuration']['distance']['text'];
                                $textInfo = '<i class="marker icon"></i> '.$schedule['schedule'][$i]['objekwisata']['objek_wisata']['alamat'];
                                $iconKiri = '<i class="big tree icon"></i>';
                            } elseif (array_key_exists(0, $schedule['schedule'][$i]) && $schedule['schedule'][$i]['tipe'] === 'last') {
                                $titleOTW = 'perjalanan ke bandara untuk pulang';
                                $titleTibaDiTujuan = 'kembali ke kota asal';
                                $durasiPerjalanan = $schedule['schedule'][$i][0]['duration']['text'];
                                $jarakPerjalanan = $schedule['schedule'][$i][0]['distance']['text'];
                                $textInfo = '<i class="big building icon"></i>'.$pesawat->order_name_detail.' - '.$pesawat->order_name;
                                $iconKiri = '<i class="big building icon"></i>';
                            }
                        }
                    @endphp
                    @if ($i === 0)
                        <li>
                            <span></span>
                            <div class="title">{{ $titleOTW }}</div>
                            <div class="info">
                                <i class="time icon"></i> {{ $durasiPerjalanan }} - {{ $jarakPerjalanan }}
                            </div>
                            <div class="time">
                                @php
                                    $time = new DateTime($schedule['schedule'][$i]['time']['go']);
                                @endphp
                                <span>
                                    {{ $time->format('d M Y') }}
                                    <br>
                                    {{ $time->format('H:i') }}
                                </span>
                            </div>
                        </li>
                    @endif
                    <li>
                        <span></span>
                        <div class="title">{{ $titleTibaDiTujuan }}</div>
                        <div class="info">
                            {!! $textInfo !!}
                        </div>
                        <div class="time">
                            @php
                                $time = new DateTime($schedule['schedule'][$i]['time']['arrive']);
                            @endphp
                            <span>
                                {{ $time->format('d M Y') }}
                                <br>
                                {{ $time->format('H:i') }}
                            </span>
                        </div>
                    </li>
                @endfor
                <li>
                    <span></span>
                    <div class="title">berangkat pulang</div>
                    <div class="info">
                        <i class="plane icon"></i> {{ $pesawat->order_name_detail }} - {{ $pesawat->order_name }}
                    </div>
                    <div class="time">
                        <span>
                            {{ date("d-m-Y", strtotime($pesawat->tgl_berangkat)) }}
                            <br>
                            {{ date("H:i", strtotime($pesawat->jam_berangkat)) }}
                        </span>
                    </div>
                </li>
                <li>
                    <span></span>
                    <div class="title">tiba di kota asal</div>
                    <div class="info">
                        <i class="plane icon"></i> {{ $pesawat->order_name_detail }} - {{ $pesawat->order_name }}
                    </div>
                    <div class="time">
                        <span>
                            {{ date("d-m-Y", strtotime($pesawat->tgl_berangkat)) }}
                            <br>
                            {{ date("H:i", strtotime($pesawat->jam_sampai)) }}
                        </span>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="ui fluid green unduh button">Unduh</div>
</div>
<div class="ui bottom attached footer"></div>
@endsection

@section('script')
<script>
    $('.unduh.button').click(function () {
        location.href = "{{ route('jadwal.download', [$id]) }}";
    });
</script>
@endsection