@extends('layouts.home')

@section('style')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://unpkg.com/fullpage.js@2.9.7/dist/jquery.fullpage.min.css">
<style>
    .ui.container.top {
        display:flex;
        justify-content:center;
        align-items:center;
        min-height:500px;
    }
</style>
@endsection

@section('content')
<div class="fullpage">
    <div class="dimmer" style="position:fixed;top:0px;bottom:0px;left:0px;right:0px;display:block;width:100vw;height:100vw;background-color:rgb(102,102,102);z-index:102;"></div>
    <div class="ui icon black top button animsition-link" style="position:fixed;bottom:50px;right:30px;">
        <i class="angle double up icon"></i>
    </div>
    <header data-anchor="top" style="background:radial-gradient(#2185d0,#141422)">
        <div class="ui container top" style="">
            <div class="ui segment">
                <div class="ui hidden divider"></div>
                    <img src=" {{ asset('let-the-story-begin.svg') }} " class="ui centered medium image" alt="">
                <div class="ui hidden divider"></div>
                <a class="ui blue left floated otomatis button">Pemilihan Paket Perjalanan Otomatis</a>
                <a class="ui yellow left floated manual button">Pemilihan Paket Perjalanan Manual</a>
            </div>
        </div>
    </header>
    <header data-anchor="otomatis">
        <div class="masthead">
            <div class="top divider home"></div>
            <div class="ui text container">
                <div class="ui blue raised segment">
                    <h2 class="ui dividing header">Pemilihan Paket Otomatis</h2>
                    <form class="ui form otomatis" action="{{ route('landingpage.simpanparameter') }}">
                        <div class="required field">
                            <label for="asal">Dari Manakah Anda Akan Berangkat?</label>
                            <div class="ui search selection dropdown">
                                <i class="plane icon"></i>
                                <input type="hidden" name="asal" data-validation="required" id="asalOtomatis">
                                <i class="dropdown icon"></i>
                                <div class="default text">Asal</div>
                                <div class="menu">
                                    @foreach ($airports as $airport)
                                        <div class="item" data-value="{{ $airport['airport_code'] }}">{{ $airport['location_name'] }} - {{ $airport['airport_name'] }}</div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="required field">
                            <label for="tujuan">Daerah Manakah Yang Ingin Anda Tuju?</label>
                            <div class="ui selection dropdown">
                                <i class="vertically flipped plane icon"></i>
                                <input type="hidden" name="tujuan" id="tujuanOtomatis" data-validation="required">
                                <i class="dropdown icon"></i>
                                <div class="default text">Daerah Tujuan</div>
                                <div class="menu">
                                    @foreach ($areas as $area)
                                        <div class="item" data-value="{{ $area->area }}">{{ $area->area }}</div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="required field">
                            <label for="rangestart">Tanggal Berapakah Anda Ingi Mulai Berlibur?</label>
                            <div class="ui calendar">
                                <div class="ui left icon input">
                                    <i class="calendar icon"></i>
                                    <input type="text" placeholder="Start" name="start" id="rangestartOtomatis"  data-validation="required" <?php // if ($dataPencarian != null && isset($dataPencarian['start'])) {echo 'value=' . $dataPencarian['start'];}?>>
                                </div>
                            </div>
                        </div>
                        <div class="required field">
                            <label for="rangeend">Tanggal Berapakah Anda Ingin Kembali?</label>
                            <div class="ui calendar">
                                <div class="ui left icon input">
                                    <i class="calendar icon"></i>
                                    <input type="text" placeholder="Akhir Liburan" name="end" id="rangeendOtomatis" data-validation="required" disabled <?php // if ($dataPencarian != null && isset($dataPencarian['end'])) {echo 'value=' . $dataPencarian['end'];}?>>
                                </div>
                            </div>
                        </div>
                        <div class="required field">
                            <label for="dewasa">Berapakah Orang Dewasa Yang Ikut Berlibur?</label>
                            <div class="ui action input dewasa otomatis">
                                <div class="ui left icon input">
                                    <i class="user icon"></i>
                                    <input placeholder="Jumlah Wisatawan" type="text" name="dewasa" value="1" data-validation="required" id="dewasaOtomatis" <?php // if ($dataPencarian != null && isset($dataPencarian['dewasa'])) {echo 'value=' . $dataPencarian['dewasa'];}?>>
                                </div>
                                <div class="ui icon minus button">
                                    <i class="minus icon"></i>
                                </div>
                                <div class="ui icon plus button">
                                    <i class="plus icon"></i>
                                </div>
                            </div>
                        </div>
                        <div class="required field">
                            <label for="anak">Berapakah Anak-Anak (2-12 Tahun) Yang Ikut Berlibur?</label>
                            <div class="ui action input anak otomatis">
                                <div class="ui left icon input">
                                    <i class="user icon"></i>
                                    <input placeholder="Jumlah Wisatawan" type="text" name="anak" value="0" id="anakOtomatis" data-validation="required" <?php // if ($dataPencarian != null && isset($dataPencarian['anak'])) {echo 'value=' . $dataPencarian['anak'];}?>>
                                </div>
                                <div class="ui icon minus button">
                                    <i class="minus icon"></i>
                                </div>
                                <div class="ui icon plus button">
                                    <i class="plus icon"></i>
                                </div>
                            </div>
                        </div>
                        <div class="required field">
                            <label for="batita">Berapakah Batita (0-2 Tahun) Yang Ikut Berlibur?</label>
                            <div class="ui action input batita otomatis">
                                <div class="ui left icon input">
                                    <i class="user icon"></i>
                                    <input placeholder="Jumlah Wisatawan" type="text" name="batita" value="0" id="batitaOtomatis" data-validation="required" <?php // if ($dataPencarian != null && isset($dataPencarian['batita'])) {echo 'value=' . $dataPencarian['batita'];}?>>
                                </div>
                                <div class="ui icon minus button">
                                    <i class="minus icon"></i>
                                </div>
                                <div class="ui icon plus button">
                                    <i class="plus icon"></i>
                                </div>
                            </div>
                        </div>
                        <div class="required field">
                            <label for="tipe">Tipe Objek Seperti Apakah Yang Anda Sukai?</label>
                            <div class="ui selection dropdown">
                                <i class="tags icon"></i>
                                <input type="hidden" name="tipe" id="tipeOtomatis" data-validation="required">
                                <i class="dropdown icon"></i>
                                <div class="default text">Tipe Objek Wisata</div>
                                <div class="menu">
                                    @foreach ($tipes as $tipe)
                                        <div class="item" data-value="{{ $tipe->id }}" >{{ $tipe->nama_tipe }}</div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="required field">
                            <label for="dana">Berapakah Jumlah Dana Yang Anda Miliki??</label>
                            <div class="ui action input dana otomatis">
                                <div class="ui left icon input">
                                    <i class="money icon"></i>
                                    <input placeholder="Jumlah Dana" value="0" type="text" name="dana" id="danaOtomatis" data-validation="required" disabled <?php // if ($dataPencarian != null && isset($dataPencarian['dana'])) {echo 'value=' . $dataPencarian['dana'];}?>>
                                </div>
                                <div class="ui icon minus button">
                                    <i class="minus icon"></i>
                                </div>
                                <div class="ui icon plus button">
                                    <i class="plus icon"></i>
                                </div>
                            </div>
                        </div>
                        <div class="inline fields">
                            <div class="field">
                                <label for="mobil">Apakah Anda Ingin Menyewa Mobil Juga?</label>
                                <div class="ui toggle checkbox">
                                    <input type="radio" name="mobil" value="0" checked>
                                    <label>Tidak</label>
                                </div>
                            </div>
                            <div class="field">
                                <div class="ui toggle checkbox">
                                    <input type="radio" name="mobil" value="1">
                                    <label>Ya</label>
                                </div>
                            </div>
                            <div class="field">
                                <div class="ui toggle checkbox">
                                    <input type="radio" name="mobil" value="2">
                                    <label>Ya, Beserta Sopir</label>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <input type="hidden" name="type" value="otomatis">
                            <input type="submit" class="ui yellow submit fluid button" value="Submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="ui bottom attached footer"></div>
    </header>
    <header data-anchor="manual">
        <div class="masthead">
            <div class="top divider home"></div>
            <div class="ui container">
                <div class="ui blue raised segment">
                    <h2 class="ui dividing header">Pemilihan Paket Manual</h2>
                    <form class="ui form manual" action="{{ route('landingpage.simpanparameter') }}">
                        <div class="three fields">
                            <div class="required field">
                                <label for="asal">Keberangkatan</label>
                                <div class="ui search selection dropdown">
                                    <i class="plane icon"></i>
                                    <input type="hidden" name="asal" data-validation="required" id="asal">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Bandara Asal</div>
                                    <div class="menu">
                                        @foreach ($airports as $airport)
                                            <div class="item" data-value="{{ $airport['airport_code'] }}">{{ $airport['location_name'] }} - {{ $airport['airport_name'] }}</div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="required field">
                                <label for="tujuan">Daerah Tujuan</label>
                                <div class="ui selection dropdown">
                                    <i class="vertically flipped plane icon"></i>
                                    <input type="hidden" name="tujuan" id="tujuan" data-validation="required">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Daerah Tujuan</div>
                                    <div class="menu">
                                        @foreach ($areas as $area)
                                            <div class="item" data-value="{{ $area->area }}">{{ $area->area }}</div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="required field">
                                <label for="dana">Jumlah Dana</label>
                                <div class="ui action input dana manual">
                                    <div class="ui left icon input">
                                        <i class="money icon"></i>
                                        <input placeholder="Jumlah Dana" type="text" name="dana" id="dana" value="0" data-validation="required" disabled <?php //if ($dataPencarian != null && isset($dataPencarian['dana'])) {echo 'value=' . $dataPencarian['dana'];}?>>
                                    </div>
                                    <div class="ui icon minus button">
                                        <i class="minus icon"></i>
                                    </div>
                                    <div class="ui icon plus button">
                                        <i class="plus icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="required field">
                                <label for="rangestart">Tanggal Liburan</label>
                                <div class="ui calendar">
                                    <div class="ui left icon input">
                                        <i class="calendar icon"></i>
                                        <input type="text" placeholder="Start" name="start" id="rangestart"  data-validation="required" <?php //if ($dataPencarian != null && isset($dataPencarian['start'])) {echo 'value=' . $dataPencarian['start'];}?>>
                                    </div>
                                </div>
                            </div>
                            <div class="required field">
                                <label for="rangeend">Akhir Liburan</label>
                                <div class="ui calendar">
                                    <div class="ui left icon input">
                                        <i class="calendar icon"></i>
                                        <input type="text" placeholder="Akhir Liburan" name="end" id="rangeend" data-validation="required" disabled <?php //if ($dataPencarian != null && isset($dataPencarian['end'])) {echo 'value=' . $dataPencarian['end'];}?>>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="three fields">
                            <div class="required field">
                                <label for="dewasa">Dewasa</label>
                                <div class="ui action input dewasa manual">
                                    <div class="ui left icon input">
                                        <i class="user icon"></i>
                                        <input placeholder="Jumlah Wisatawan" type="text" name="dewasa" value="1" data-validation="required" id="dewasa" <?php //if ($dataPencarian != null && isset($dataPencarian['dewasa'])) {echo 'value=' . $dataPencarian['dewasa'];}?>>
                                    </div>
                                    <div class="ui icon minus button">
                                        <i class="minus icon"></i>
                                    </div>
                                    <div class="ui icon plus button">
                                        <i class="plus icon"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="required field">
                                <label for="anak">Anak-Anak (2-12 Tahun)</label>
                                <div class="ui action input anak manual">
                                    <div class="ui left icon input">
                                        <i class="user icon"></i>
                                        <input placeholder="Jumlah Wisatawan" type="text" name="anak" value="0" id="anak" data-validation="required" <?php //if ($dataPencarian != null && isset($dataPencarian['anak'])) {echo 'value=' . $dataPencarian['anak'];}?>>
                                    </div>
                                    <div class="ui icon minus button">
                                        <i class="minus icon"></i>
                                    </div>
                                    <div class="ui icon plus button">
                                        <i class="plus icon"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="required field">
                                <label for="batita">Batita (0-2 Tahun)</label>
                                <div class="ui action input batita manual">
                                    <div class="ui left icon input">
                                        <i class="user icon"></i>
                                        <input placeholder="Jumlah Wisatawan" type="text" name="batita" value="0" id="batita" data-validation="required" <?php //if ($dataPencarian != null && isset($dataPencarian['batita'])) {echo 'value=' . $dataPencarian['batita'];}?>>
                                    </div>
                                    <div class="ui icon minus button">
                                        <i class="minus icon"></i>
                                    </div>
                                    <div class="ui icon plus button">
                                        <i class="plus icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <input type="hidden" name="type" value="manual">
                            <input type="submit" class="ui yellow fluid button" value="Submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="ui bottom attached footer"></div>
    </header>
</div>
<div class="ui modal login">
    <i class="close icon"></i>
    <div class="content">
        <form class="ui form login" action="{{ route('login') }}">
            <div class="field">
                <label>Email</label>
                <input type="text" name="email" id="email" placeholder="Email">
            </div>
            <div class="field">
                <label>Password</label>
                <input type="password" name="password" id="password">
            </div>
        </form>
    </div>
    <div class="actions">
        <a href="{{ route('register') }}" class="ui blue button">Daftar Akun</a>
        <div class="ui yellow approve button">
            Submit
        </div>
    </div>
</div>
<div class="ui wisatawan otomatis scrolling modal">
    <div class="header">
        Data Wisatawan
    </div>
    <div class="content wisatawan">
    </div>
    <div class="actions">
        <div class="ui red approve button">
            Simpan
        </div>
        <div class="ui blue deny button">
            Batal
        </div>
    </div>
</div>
<div class="ui progressbar modal">
    <div class="header">Sedang Memproses Pemilihan Paket Perjalanan Otomatis</div>
    <div class="content">
        <div class="ui active blue progress" data-value="0" data-total="4">
            <div class="bar">
                <div class="progress"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://unpkg.com/fullpage.js@2.9.7/vendors/scrolloverflow.min.js"></script>
<script src="https://unpkg.com/fullpage.js@2.9.7/dist/jquery.fullpage.min.js"></script>
<script src="https://unpkg.com/event-source-polyfill@^0.0.11/src/eventsource.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.7/jquery.fullpage.extensions.min.js"></script>
<script src="https://unpkg.com/accounting@0.4.1/accounting.js"></script>

<script>
    $('.inverted.menu').animate({
        backgroundColor:'rgba(33, 133, 208, 0)'
    }, 100);
    $('.top.button').hide();
    $('.dimmer').fadeOut(200);
    $('.ui.dropdown').dropdown();
    $('.dana.otomatis .button, .dana.manual .button').addClass('disabled');
    $('#dana, #danaOtomatis, #dewasa, #dewasaOtomatis, #anak, #anakOtomatis, #batita, #batitaOtomatis').focus(function(e) {
        e.preventDefault();
        $(this).blur();
    });

    $('.dewasa.otomatis .plus.button').on('click', function () {
        $('.dewasa.otomatis #dewasaOtomatis').val(parseInt($('.dewasa.otomatis #dewasaOtomatis').val()) + 1);
    });

    $('.dewasa.otomatis .minus.button').on('click', function () {
        let val = parseInt($('.dewasa.otomatis #dewasaOtomatis').val());
        if (val > 1) {
            val = val - 1;
        }
        if (val == 1) {
            val = val;
        }
        if (val < 1) {
            val = 1;
        }
        $('.dewasa.otomatis #dewasaOtomatis').val(val);
    });

    $('.anak.otomatis .plus.button').on('click', function () {
        $('.anak.otomatis #anakOtomatis').val(parseInt($('.anak.otomatis #anakOtomatis').val()) + 1);
    });

    $('.anak.otomatis .minus.button').on('click', function () {
        let val = parseInt($('.anak.otomatis #anakOtomatis').val());
        if (val > 0) {
            val = val - 1;
        }
        if (val == 0) {
            val = val;
        }
        if (val < 0) {
            val = 0;
        }
        $('.anak.otomatis #anakOtomatis').val(val);
    });

    $('.batita.otomatis .plus.button').on('click', function () {
        $('.batita.otomatis #batitaOtomatis').val(parseInt($('.batita.otomatis #batitaOtomatis').val()) + 1);
    });

    $('.batita.otomatis .minus.button').on('click', function () {
        let val = parseInt($('.batita.otomatis #batitaOtomatis').val());
        if (val > 0) {
            val = val - 1;
        }
        if (val == 0) {
            val = val;
        }
        if (val < 0) {
            val = 0;
        }
        $('.batita.otomatis #batitaOtomatis').val(val);
    });

    $('.dewasa.manual .plus.button').on('click', function () {
        $('.dewasa.manual #dewasa').val(parseInt($('.dewasa.manual #dewasa').val()) + 1);
    });

    $('.dewasa.manual .minus.button').on('click', function () {
        let val = parseInt($('.dewasa.manual #dewasa').val());
        if (val > 1) {
            val = val - 1;
        }
        if (val == 1) {
            val = val;
        }
        if (val < 1) {
            val = 1;
        }
        $('.dewasa.manual #dewasa').val(val);
    });

    $('.anak.manual .plus.button').on('click', function () {
        $('.anak.manual #anak').val(parseInt($('.anak.manual #anak').val()) + 1);
    });

    $('.anak.manual .minus.button').on('click', function () {
        let val = parseInt($('.anak.manual #anak').val());
        if (val > 0) {
            val = val - 1;
        }
        if (val == 0) {
            val = val;
        }
        if (val < 0) {
            val = 0;
        }
        $('.anak.manual #anak').val(val);
    });

    $('.batita.manual .plus.button').on('click', function () {
        $('.batita.manual #batita').val(parseInt($('.batita.manual #batita').val()) + 1);
    });

    $('.batita.manual .minus.button').on('click', function () {
        let val = parseInt($('.batita.manual #batita').val());
        if (val > 0) {
            val = val - 1;
        }
        if (val == 0) {
            val = val;
        }
        if (val < 0) {
            val = 0;
        }
        $('.batita.manual #batita').val(val);
    });

    var today = new Date();
    $.datetimepicker.setLocale('id');
    $('#rangestart').datetimepicker({
        timepicker:false,
        format:'d-m-Y',
        formatDate:'d-m-Y',
        onChangeDateTime:function(ct,$i){
        if (ct!=null) {
            $('#rangeend').datetimepicker('setOptions',{mindate:this.val()?$('#rangestart').val():0,value:ct});
        }
        $('#rangeend').prop('disabled',false);
        },
        minDate:0,
        allowBlank:true,
        style:'border-radius:.28571429rem'
    }).focus(function(e) {
        e.preventDefault();
        $(this).blur();
    });
    $('#rangestartOtomatis').datetimepicker({
        timepicker:false,
        format:'d-m-Y',
        formatDate:'d-m-Y',
        onChangeDateTime:function(ct,$i){
        if (ct!=null) {
            $('#rangeendOtomatis').datetimepicker('setOptions',{mindate:this.val()?$('#rangestartOtomatis').val():0,value:ct});
        }
        $('#rangeendOtomatis').prop('disabled',false);
        },
        minDate:0,
        allowBlank:true,
        style:'border-radius:.28571429rem'
    }).focus(function(e) {
        e.preventDefault();
        $(this).blur();
    });
    $('#rangeend').datetimepicker({
        timepicker:false,
        format:'d-m-Y',
        formatDate:'d-m-Y',
        onShow:function(ct){
        this.setOptions({
            minDate: $('#rangestart').val()?$('#rangestart').val():0,
        })
        },
        onChangeDateTime:function(ct,$i) {
            var startValue=$('#rangestart').datetimepicker('getValue');
            var start=new Date(startValue.getFullYear(), startValue.getMonth(), startValue.getDate());
            diff=(ct-start)/(1000*60*60*24);
            $('#dana').prop('disabled',false);
            $('.dana.manual .button').removeClass('disabled');
        },
        allowBlank:true,
        style:'border-radius:.28571429rem'
    }).focus(function(e) {
        e.preventDefault();
        $(this).blur();
    });
    $('#rangeendOtomatis').datetimepicker({
        timepicker:false,
        format:'d-m-Y',
        formatDate:'d-m-Y',
        onShow:function(ct){
            this.setOptions({
                minDate: $('#rangestartOtomatis').val()?$('#rangestartOtomatis').val():0,
            })
        },
        onChangeDateTime:function(ct,$i) {
            var startValue=$('#rangestartOtomatis').datetimepicker('getValue');
            var start=new Date(startValue.getFullYear(), startValue.getMonth(), startValue.getDate());
            diffOtomatis=(ct-start)/(1000*60*60*24);
            $('#danaOtomatis').prop('disabled',false);
            $('.dana.otomatis .button').removeClass('disabled');
        },
        allowBlank:true,
        style:'border-radius:.28571429rem'
    }).focus(function(e) {
        e.preventDefault();
        $(this).blur();
    });

    $('.dana.otomatis .plus.button').on('click', function () {
        let wisatawan = parseInt($('.batita.otomatis #batitaOtomatis').val()) + parseInt($('.anak.otomatis #anakOtomatis').val()) + parseInt($('.dewasa.otomatis #dewasaOtomatis').val());
        let minimalDana = (diffOtomatis * wisatawan) * 1000000;
        let val = accounting.unformat($('.dana.otomatis #danaOtomatis').val()) * 1000000;
        if (val < minimalDana) {
            val = minimalDana;
        } else {
            val = val + 100000;
        }
        $('.dana.otomatis #danaOtomatis').val(accounting.formatMoney(val, '', 0, '.'));
    });
    $('.dana.otomatis .minus.button').on('click', function () {
        let wisatawan = parseInt($('.batita.otomatis #batitaOtomatis').val()) + parseInt($('.anak.otomatis #anakOtomatis').val()) + parseInt($('.dewasa.otomatis #dewasaOtomatis').val());
        let minimalDana = (diffOtomatis * wisatawan) * 1000000;
        let val = accounting.unformat($('.dana.otomatis #danaOtomatis').val()) * 1000000;
        if (val >= minimalDana) {
            val = val - 100000;
        }
        if (val < minimalDana) {
            val = minimalDana;
        }
        $('.dana.otomatis #danaOtomatis').val(accounting.formatMoney(val, '', 0, '.'));
    });
    $('.dana.manual .plus.button').on('click', function () {
        let wisatawan = parseInt($('.batita.manual #batita').val()) + parseInt($('.anak.manual #anak').val()) + parseInt($('.dewasa.manual #dewasa').val());
        let minimalDana = (diff * wisatawan) * 1000000;
        let val = accounting.unformat($('.dana.manual #dana').val()) * 1000000;
        if (val < minimalDana) {
            val = minimalDana;
        } else {
            val = val + 100000;
        }
        $('.dana.manual #dana').val(accounting.formatMoney(val, '', 0, '.'));
    });
    $('.dana.manual .minus.button').on('click', function () {
        let wisatawan = parseInt($('.batita.manual #batita').val()) + parseInt($('.anak.manual #anak').val()) + parseInt($('.dewasa.manual #dewasa').val());
        let minimalDana = (diff * wisatawan) * 1000000;
        let val = accounting.unformat($('.dana.manual #dana').val()) * 1000000;
        if (val >= minimalDana) {
            val = val - 100000;
        }
        if (val < minimalDana) {
            val = minimalDana;
        }
        $('.dana.manual #dana').val(accounting.formatMoney(val, '', 0, '.'));
    });
    // window.onerror=function() {
    //     return true;
    // }

    $(document).ready(function () {
        $('.fullpage').fullpage({
            scrollingSpeed:800,
            sectionSelector:'header',
            easing:'easeInExpo',
            fixedElements:'.top.button, .inverted.menu, .dimmer',
            keyboardScrolling:false,
            scrollOverflow:true,
            recordHistory:false,
            controlArrows:false,
            lockAnchors:true,
            onLeave:function(index, nextIndex, direction) {
                $('.dimmer').fadeIn();
                if (nextIndex==1) {
                    $('.top.button').fadeToggle();
                    $('.inverted.menu').animate({
                        backgroundColor:'rgba(33, 133, 208, 0)'
                    }, 500);
                } else {
                    $('.top.button').fadeToggle();
                    $('.inverted.menu').animate({
                        backgroundColor:'rgba(33, 133, 208, 1)'
                    }, 500);
                }
            },
            afterLoad:function(anchorLink, index) {
                $('.dimmer').fadeOut();
            },
            afterRender: function () {
                $('.item.login').on('click', function () {
                    loginModal(true);
                })
            }
        });
        $.fn.fullpage.setAllowScrolling(false);
        $('.top.button').click(function () {
            $.fn.fullpage.moveTo(1);
        });
        $('.otomatis.button').click(function () {
            $.fn.fullpage.moveTo(2);
        });
        $('.manual.button').click(function () {
            $.fn.fullpage.moveTo(3);
        });
        $('.ui.form.manual, .ui.form.otomatis').submit(function(e) {
            e.preventDefault();
        });
        $.validate({
            form:'.ui.form.manual',
            errorMessagePosition:'top',
            validateOnBlur:false,
            errorElementClass:'error',
            errorMessageClass:'ui negative message',
            validateHiddenInputs:true,
            onSuccess:function() {
                if (!$loggedIn) {
                    loginModal(false);
                } else {
                    axios.post($('.ui.form.manual').attr('action'), $('.ui.form.manual').serialize())
                        .then((response) => {
                            location.replace("{{ route('home.rekap') }}");
                        })
                        .catch((error) => {
                            iziToast.error({
                                title: 'Action Failed',
                                message: error.response.data.message
                            });
                            delay(1000);
                            $('.ui.login.modal').modal('hide');
                        });
                }
            }
        });
        $.validate({
            form:'.ui.form.otomatis',
            errorMessagePosition:'top',
            validateOnBlur:false,
            errorElementClass:'error',
            errorMessageClass:'ui negative message',
            validateHiddenInputs:true,
            onSuccess:function() {
                if (!$loggedIn) {
                    loginModal(false, true);
                } else {
                    tambahWisatawan();
                }
            }
        });
    });

    function loginModal(reload = false, otomatis = false) {
        $('.ui.login.modal').modal({
            closable: false,
            onApprove: function () {
                $('.ui.button').addClass('loading');
                axios.post($('.ui.form.login').attr('action'), $('.ui.form.login').serialize())
                    .then(function (response) {
                        if (reload) {
                            location.href = response.data.redirect;
                        }

                        login(true);

                        if (!otomatis) {
                            $('.ui.form.manual').submit();
                        }

                        if (otomatis) {
                            tambahWisatawan();
                        }

                        $('.ui.button').removeClass('loading');
                        $('.ui.login.modal').modal('hide');
                    })
                    .catch((error) => {
                        $('.ui.button').removeClass('loading');
                        $('.ui.login.modal').modal('hide');
                        iziToast.error({
                            title: 'Action Failed',
                            message: error.response.data.message
                        });
                    });
                return false;
            }
        }).modal('show');
    }

    function login(params = false) {
        $loggedIn = params;
    }

    function tambahWisatawan() {
        let jumlah = Number($('#dewasaOtomatis').val()) + Number($('#anakOtomatis').val()) + Number($('#batitaOtomatis').val());
        let jumlahDewasa = Number($('#dewasaOtomatis').val());
        let jumlahAnak = Number($('#anakOtomatis').val());
        let jumlahBatita = Number($('#batitaOtomatis').val());
        $('.content.wisatawan').empty();

        $('.content.wisatawan').append("<form class=\"ui form wisawatan\" action=\"{{ route('home.tambahwisatawan') }}\">");
        for (i=0;i<jumlahDewasa;i++) {
            $('.form.wisawatan').append(
                '<div class=\"ui blue inverted attached segment\">'+
                '<h3 class=\"ui header\">Data Penumpang Dewasa '+Number(i+1)+'</h3>'+
                '</div>'+
                '<div class=\"ui attached segment\">'+
                '<div class=\"four fields\">'+
                    '<div class=\"field\">'+
                    '<label>Title</label>'+
                    '<select class=\"ui dropdown\" name=\"title[]\">'+
                        '<option value=\"\">Title</option>'+
                        '<option value=\"Mr\">Bapak</option>'+
                        '<option value=\"Mrs\">Ibu</option>'+
                        '<option value=\"Ms\">Nona</option>'+
                    '</select>'+
                    '</div>'+
                    '<div class=\"field\">'+
                    '<label>Nama Depan</label>'+
                    '<input type=\"text\" name=\"nd[]\" placeholder=\"Nama Depan\">'+
                    '</div>'+
                    '<div class=\"field\">'+
                    '<label>Nama Belakang</label>'+
                    '<input type=\"text\" name=\"nb[]\" placeholder=\"Nama Belakang\">'+
                    '</div>'+
                    '<div class=\"field\">'+
                    '<label>Tanggal Lahir</label>'+
                    '<div class=\"ui calendar\">'+
                        '<div class=\"ui left icon input\">'+
                        '<i class=\"calendar icon\"></i>'+
                        '<input type=\"text\" name=\"tgl[]\" placeholder=\"Tanggal Lahir\" id=\"tgl\">'+
                        '</div>'+
                    '</div>'+
                    '</div>'+
                '</div>'+
                '</div>'
            );
        }

        for (i=0;i<jumlahAnak;i++) {
            $('.form.wisawatan').append(
                '<div class=\"ui blue inverted attached segment\">'+
                '<h3 class=\"ui header\">Data Penumpang Anak-Anak '+Number(i+1)+'</h3>'+
                '</div>'+
                '<div class=\"ui attached segment\">'+
                '<div class=\"four fields\">'+
                    '<div class=\"field\">'+
                    '<label>Title</label>'+
                    '<select class=\"ui dropdown\" name=\"title[]\">'+
                        '<option value=\"\">Title</option>'+
                        '<option value=\"Mstra\">Anak Laki-Laki</option>'+
                        '<option value=\"Missa\">Anak Perempuan</option>'+
                    '</select>'+
                    '</div>'+
                    '<div class=\"field\">'+
                    '<label>Nama Depan</label>'+
                    '<input type=\"text\" name=\"nd[]\" placeholder=\"Nama Depan\">'+
                    '</div>'+
                    '<div class=\"field\">'+
                    '<label>Nama Belakang</label>'+
                    '<input type=\"text\" name=\"nb[]\" placeholder=\"Nama Belakang\">'+
                    '</div>'+
                    '<div class=\"field\">'+
                    '<label>Tanggal Lahir</label>'+
                    '<div class=\"ui calendar\">'+
                        '<div class=\"ui left icon input\">'+
                        '<i class=\"calendar icon\"></i>'+
                        '<input type=\"text\" name=\"tgl[]\" placeholder=\"Tanggal Lahir\" id=\"tgl\">'+
                        '</div>'+
                    '</div>'+
                    '</div>'+
                '</div>'+
                '</div>'
            );
        }

        for (i=0;i<jumlahBatita;i++) {
            $('.form.wisawatan').append(
                '<div class=\"ui blue inverted attached segment\">'+
                '<h3 class=\"ui header\">Data Penumpang Batita '+Number(i+1)+'</h3>'+
                '</div>'+
                '<div class=\"ui attached segment\">'+
                '<div class=\"four fields\">'+
                    '<div class=\"field\">'+
                    '<label>Title</label>'+
                    '<select class=\"ui dropdown\" name=\"title[]\">'+
                        '<option value=\"\">Title</option>'+
                        '<option value=\"Mstrb\">Batita Laki-Laki</option>'+
                        '<option value=\"Missb\">Batita Perempuan</option>'+
                    '</select>'+
                    '</div>'+
                    '<div class=\"field\">'+
                    '<label>Nama Depan</label>'+
                    '<input type=\"text\" name=\"nd[]\" placeholder=\"Nama Depan\">'+
                    '</div>'+
                    '<div class=\"field\">'+
                    '<label>Nama Belakang</label>'+
                    '<input type=\"text\" name=\"nb[]\" placeholder=\"Nama Belakang\">'+
                    '</div>'+
                    '<div class=\"field\">'+
                    '<label>Tanggal Lahir</label>'+
                    '<div class=\"ui calendar\">'+
                        '<div class=\"ui left icon input\">'+
                        '<i class=\"calendar icon\"></i>'+
                        '<input type=\"text\" name=\"tgl[]\" placeholder=\"Tanggal Lahir\" id=\"tgl\">'+
                        '</div>'+
                    '</div>'+
                    '</div>'+
                '</div>'+
                '</div>'
            );
        }

        $('.form.wisawatan').append('<input type=\"hidden\" name=\"jumlah\" value=\"'+jumlah+'\">');
        $('.content.wisatawan').append('</form>');
        $('.dropdown').dropdown();
        $('[id=tgl]').datetimepicker({
            timepicker:false,
            format:'d-m-Y',
            style:'border-radius:.28571429rem'
        }).focus(function(e) {
            e.preventDefault();
            $(this).blur();
        });

        $('.wisatawan.otomatis.modal').modal('setting','closable',false).modal({
            onApprove:function () {
                $('.wisatawan.otomatis.modal .button').addClass('loading');
                // simpan wisatawan
                axios.post($('.content.wisatawan').children().attr('action'), $('.content.wisatawan').children().serialize())
                    .then((response) => {
                        // simpan parameter otomatis
                        axios.post($('.ui.form.otomatis').attr('action'), $('.ui.form.otomatis').serialize())
                            .then((response) => {
                                // bikin pilihan otomatis
                                $('.wisatawan.otomatis.modal').modal('hide');
                                $('.wisatawan.otomatis.modal .button').removeClass('loading');
                                setTimeout(prosesOtomatis(), 500);
                            })
                            .catch((error) => {
                                $('.wisatawan.otomatis.modal').modal('hide');
                                $('.wisatawan.otomatis.modal .button').removeClass('loading');
                                iziToast.error({
                                    title: 'Action Failed',
                                    message: error.response.data.message
                                });
                            });
                        // location.replace("{{ route('home.rekap') }}");
                    })
                    .catch((error) => {
                        $('.wisatawan.otomatis.modal').modal('hide');
                        $('.wisatawan.otomatis.modal .button').removeClass('loading');
                        iziToast.error({
                            title: 'Action Failed',
                            message: error.response.data.message
                        });
                    });

                return false;
            }
        }).modal('show');
    }

    function prosesOtomatis() {
        $('.progressbar.modal').modal({
            closable:false
        }).modal('show');

        $('.blue.progress').progress('reset');
        let eventSource = new EventSource("{{ route('otomatis') }}");

        eventSource.onopen = function () {
            console.log('opened');
        }

        eventSource.onmessage = function (event) {
            $('.blue.progress').progress('set progress', event.data);

            if(typeof(event.error) != undefined && event.error != null) {
                eventSource.close();
                iziToast.error({
                    title: 'Action Failed',
                    message: event.error
                });
                $('.progressbar.modal').modal('hide');
            };

            if (event.data == 4) {
                eventSource.close();
                console.log('komplit');
                window.location = "{{ route('home.rekap') }}";
            };

            console.log(event.data);
        }

        eventSource.onerror = function (event) {
            console.log(event);
            eventSource.close();
            $('.blue.progress').progress('reset');
            $('.progressbar.modal').modal('hide');
            iziToast.error({
                title: 'Action Failed',
                message: 'Proses Pencarian Gagal'
            });
        }
    }
</script>
@endsection