@extends('layouts.home')

@section('content')
<div class="ui container content">
    <div class="top divider"></div>
    <div class="top divider"></div>
    <div class="ui grid">
        <div class="sixteen wide column">
            <div class="ui segments">
                <div class="ui segment">
                    <h3>Alokasi Dana Permalam: Rp. {{ number_format($alokasi, 0, ',', '.') }}</h3>
                    <a class="ui right floated yellow button" style="margin-top:-35px;" href="{{ route('landingpage.index') }}">Ubah</a>
                </div>
                <div class="ui segment mydimmer" id="segmentHotel">
                    @foreach ($tujuans as $tujuan)
                        <div class="ui divided items" id="hotel" data-tujuan="{{ $tujuan->area }}" data-lat="{{ $tujuan->lat }}" data-lng="{{ $tujuan->lng }}"></div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ui bottom attached footer"></div>
@endsection

@section('script')
<script>
    $('.ui.page.dimmer').dimmer('show');
    $(function() {
        setTimeout(loadHotel, 10);
    });

    function loadHotel() {
        axios.post("{{ route('cari.hotel.get') }}")
            .then((response) => {
                $('#hotel').html(response.data);
                $('.ui.rating').rating({
                    maxRating: 5
                }).rating('disable');

                $('.detail.button').click(function() {
                    var url=$(this).data('url');

                    axios.post("{{ route('cari.hotel.get', ['detail']) }}", {
                        url: url
                    }).then((response) => {
                        location.href = "{{ route('cari.hotel.detail') }}"
                    }).catch((error) => {
                        $('.ui.page.dimmer').dimmer('hide');

                        iziToast.error({
                            title: 'Action Failed',
                            message: error.response.data
                        });
                    })
                })

                $('.ui.page.dimmer').dimmer('hide');
            }).catch((error) => {
                $('.ui.page.dimmer').dimmer('hide');

                iziToast.error({
                    title: 'Action Failed',
                    message: error.response.data
                });
            })
    }
</script>
@endsection