@extends('layouts.home')

@section('content')
<div class="ui container content">
    <div class="top divider"></div>
    <div class="top divider"></div>
    <div class="ui grid">
        <div class="sixteen wide column">
            <div class="ui segments">
                <div class="ui clearing segment">
                    <h3 class="ui left floated header">Sisa Dana: Rp. {{ number_format($sisaDana, 0, '.', ',') }}</h3>
                    <a class="ui right floated red button">Batal</a>
                </div>
                @if ($minPrice > $sisaDana)
                    <div class="ui negative message">
                        <div class="header">
                            Alokasi Dana Tidak Cukup Untuk Menyewa Mobil
                        </div>
                    </div>
                @else
                    @foreach ($mobils as $mobil)
                        <div class="ui clearing segment">
                            <div class="ui hidden divider"></div>
                            <div class="ui small image" style="float:left;">
                                <img src="{{ asset($mobil->image_path) }}" alt="Gambar {{ $mobil->model }}">
                            </div>
                            <div class="content" style="padding-left:160px;">
                                <h3 class="ui header">
                                {{ $mobil->model }}
                                <div class="ui sub header">
                                    <i class="user icon"></i>
                                    {{ $mobil->jumlah_penumpang }} Orang
                                </div>
                                </h3>
                                <div class="ui two column relaxed grid">
                                    <div class="ui center aligned column">
                                        <b>Tanpa Supir: {{ 'Rp '.number_format($mobil->harga_kosongan, 0, '.', ',') }}</b>
                                        <br>
                                        <a class="ui blue pesan button" data-model="{{ $mobil->model }}" data-supir="false" data-biaya="{{ $mobil->harga_kosongan }}" data-id="{{ $mobil->id }}">Pesan</a>
                                    </div>
                                    <div class="ui vertical divider" style="left:50%;height:calc(30% - 1rem);transform:translateX(50%);">Atau</div>
                                    <div class="ui center aligned column">
                                        <b>Dengan supir + Bensin: {{ 'Rp '.number_format($mobil->harga_supir_bensin, 0, '.', ',') }}</b>
                                        <br>
                                        <a class="ui blue pesan button" data-model="{{ $mobil->model }}"  data-supir="true" data-biaya="{{ $mobil->harga_supir_bensin }}" data-id="{{ $mobil->id }}">Pesan</a>
                                    </div>
                                </div>
                            </div>
                            <div class="ui hidden divider"></div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <div class="ui bottom attached footer"></div>
</div>
<div class="ui small pesan modal">
    <div class="header">
        Apakah Anda Yakin Akan Memesan Mobil <span id="model"></span><span id="driver"></span>?
    </div>
    <div class="actions">
        <div class="ui blue approve button">
            Pesan
        </div>
        <div class="ui red cancel button">
            Batal
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('.blue.pesan.button').on('click',function() {
        var model = $(this).data('model'), id=$(this).data('id'), biaya=$(this).data('biaya'), driver=$(this).data('supir');

        $('#model').text(model);

        if (driver) {
            $('#driver').text(' Beserta Supir');
        } else {
            $('#driver').text('');
        }

        $('.ui.small.pesan.modal').modal({
            onApprove:function() {
                $('.ui.page.dimmer').dimmer('show');
                axios.post("{{ route('cari.mobil.simpan') }}", {
                    idMobil:id,
                    harga:biaya,
                    supir:driver
                }).then((response) => {
                    location.href = "{{ route('home.rekap') }}"

                    return false;
                    // $('.ui.page.dimmer').dimmer('hide');
                }).catch((error) => {
                    $('.ui.page.dimmer').dimmer('hide');

                    iziToast.error({
                        title: 'Action Failed',
                        message: error.response.data
                    });

                    return true;
                })
            }
        }).modal('show');
    })
</script>
@endsection