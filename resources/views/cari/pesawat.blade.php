@extends('layouts.home')

@section('content')
<div class="ui container content">
    <div class="top divider"></div>
    <div class="top divider"></div>
    <div class="ui grid">
        <div class="sixteen wide column">
            <div class="ui segments">
                <h3 class="ui yellow inverted segment">Rangkuman Pilihan</h3>
                <div class="ui horizontal segments white">
                    <div class="ui segments to vertical">
                        <div class="ui basic square segment mydimmer" id="segmentRangkumanPergi">
                            <div class="ui items" id="rangkumanPergi">
                            </div>
                        </div>
                        <div class="ui basic square segment mydimmer" id="segmentRangkumanPulang">
                            <div class="ui items" id="rangkumanPulang">
                            </div>
                        </div>
                    </div>
                    <div class="ui basic center aligned segment mydimmer">
                        <div class="ui items">
                            <div class="ui item">
                                <div class="ui hidden divider"></div>
                            </div>
                            <div class="ui item">
                            </div>
                            <div class="ui item">
                                <div class="content">
                                    <a onclick="simpan()" class="ui yellow icon disabled button" id="rangkuman">
                                        <i class="angle double right icon"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ui item">
                                <div class="ui hidden divider"></div>
                            </div>
                            <div class="ui item">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="eight wide column">
            <div class="ui segments">
                <h3 class="ui blue inverted segment">Pergi: {{ $start->format('d M Y') }}</h3>
                <div class="ui segment mydimmer" id="segmentPergi">
                    <div class="ui divided items" id="pergi">
                        <div class='ui negative message'>
                            Tidak ada data penerbangan.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="eight wide column">
            <div class="ui segments">
                <h3 class="ui blue inverted segment">Pulang: {{ $end->format('d M Y') }}</h3>
                <div class="ui segment mydimmer" id="segmentPulang">
                    <div class="ui divided items" id="pulang">
                        <div class='ui negative message'>
                            Tidak ada data penerbangan.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ui bottom attached footer"></div>
@endsection

@section('script')
<script>
    $finish = false;
    $(function(){
        setTimeout(loadPergi,10);
        setTimeout(loadPulang,10);
        $('.ui.page.dimmer').dimmer('show');
    });

    function loadPergi() {
        axios.get("{{ route('cari.pesawat.pergi') }}")
            .then((response) => {
                $('#pergi .negative.message').remove();
                $('#pergi').html(response.data.full);
                $('#rangkumanPergi').html(response.data.min);

                if ($('#rangkumanPulang > div').length > 0 && $('#rangkumanPergi > div').length > 0) {
                    $('#rangkuman').removeClass('disabled');
                }

                if ($finish) {
                    $('.ui.page.dimmer').dimmer('hide');
                } else {
                    $finish = true;
                }
            }).catch((error) => {
                if ($finish) {
                    $('.ui.page.dimmer').dimmer('hide');
                } else {
                    $finish = true;
                }

                iziToast.error({
                    title: 'Action Failed',
                    message: error.response.data
                });
            });
    }

    function loadPulang() {
        axios.get("{{ route('cari.pesawat.pulang') }}")
            .then((response) => {
                $('#pulang .negative.message').remove();
                $('#pulang').html(response.data.full);
                $('#rangkumanPulang').html(response.data.min);

                if ($('#rangkumanPulang > div').length > 0 && $('#rangkumanPergi > div').length > 0) {
                    $('#rangkuman').removeClass('disabled');
                };

                if ($finish) {
                    $('.ui.page.dimmer').dimmer('hide');
                } else {
                    $finish = true;
                }
            }).catch((error) => {
                if ($finish) {
                    $('.ui.page.dimmer').dimmer('hide');
                } else {
                    $finish = true;
                }

                iziToast.error({
                    title: 'Action Failed',
                    message: error.response.data
                });
            });
    }
    function pilihPesawat(pp,flightId) {
        var idPP, price;
        if (pp==='pulang') {
            idPP='#rangkumanPulang';
            price='pulang';
        } else {
            idPP='#rangkumanPergi';
            price='pergi';
        }
        $(idPP).empty();
        $('#'+flightId).clone().appendTo(idPP);
        $(idPP+'>#'+flightId+'>.content>.extra>.button').addClass('selected');
        $(idPP+'>#'+flightId+'>.content>.meta>.price').addClass('selected').addClass(price);
        $('.button.selected').remove();
    }

    function simpan() {
        $('.ui.page.dimmer').dimmer('show');
        total=accounting.unformat($('.price.selected.pergi').text())+accounting.unformat($('.price.selected.pulang').text());

        axios.post("{{ route('cari.pesawat.simpan') }}", {
            idPergi: $('#rangkumanPergi > .item').attr('id'),
            tglPergi: "{{ $start->format('Y-m-d') }}",
            idPulang: $('#rangkumanPulang > .item').attr('id'),
            tglPulang: "{{ $start->format('Y-m-d') }}",
            biaya: total
        }).then((response) => {
            location.replace("{{ route('home.rekap') }}");
            // $('.ui.page.dimmer').dimmer('hide');
        }).catch((error) => {
            $('.ui.page.dimmer').dimmer('hide');

            if (error.response.data === null) {
                var res = error.response.statusText;
            } else {
                var res = error.response.data;
            }

            iziToast.error({
                title: 'Action Failed',
                message: res
            });
        });
    }
</script>
@endsection