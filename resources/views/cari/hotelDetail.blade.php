@extends('layouts.home')

@section('style')
<link rel="stylesheet" href="https://unpkg.com/nanogallery2@2.2.0/dist/css/nanogallery2.min.css">
@endsection

@section('content')
<div class="ui container content">
    <div class="top divider"></div>
    <div class="top divider"></div>
    <div class="ui grid">
        <div class="sixteen wide column">
            <div class="ui blue segment">
                <h2 class="ui dividing header">
                    {{ $hotel['breadcrumb']['business_name'] }}
                    <div class="sub header">
                        <div class="ui star rating" data-rating="{{ $hotel['breadcrumb']['star_rating'] }}"></div>{{ $hotel['general']['address'] }}
                    </div>
                </h2>
                <div class="ui segments detail">
                    <div class="ui basic segments">
                        <div class="ui segment">
                            <div class="ui embed" style="border-radius:.28571429rem;" id="map" data-lat="{{ $hotel['general']['latitude'] }}" data-lng="{{ $hotel['general']['longitude'] }}"></div>
                        </div>
                        <div class="ui segment">
                            <div class="ui images" id="nanogallery2" data-nanogallery2='{"thumbnailWidth":"auto", "thumbnailHeight":55, "viewerToolbar":{"display":false}, "locationHash": false}'>
                                @for ($i = 0; $i < count($hotel['all_photo']['photo']); $i++)
                                    <a href="{{ str_replace('.s', '.picture525x375', $hotel['all_photo']['photo'][$i]['file_name']) }}" data-ngthumb="{{ str_replace('.s', '.picture525x375', $hotel['all_photo']['photo'][$i]['file_name']) }}" data-ngdesc="" width="50" height="50"></a>
                                @endfor
                            </div>
                        </div>
                    </div>
                    <div class="ui blue inverted segment">
                        <h3 class="ui header">Fasilitas</h3>
                    </div>
                    <div class="ui segment">
                        <h3 class="ui horizontal divider header">Hotel</h3>
                        <div class="ui list" style="width:70%;margin:auto;">
                            @for ($i = 0;$i < count($hotel['avail_facilities']['avail_facilitiy']); $i++)
                                @if ($hotel['avail_facilities']['avail_facilitiy'][$i]['facility_type']=='hotel')
                                    <div class="item" style="margin-left:20px;float:left;width:45%;">
                                        <i class="checkmark icon"></i>
                                        <div class="content">
                                            <div class="description">
                                                {{ $hotel['avail_facilities']['avail_facilitiy'][$i]['facility_name'] }}
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endfor
                            <div style="clear:both;"></div>
                        </div>
                        <h3 class="ui horizontal divider header">Kamar</h3>
                        <div class="ui list" style="width:70%;margin:auto;">
                            @for ($i = 0; $i < count($hotel['avail_facilities']['avail_facilitiy']); $i++)
                                @if ($hotel['avail_facilities']['avail_facilitiy'][$i]['facility_type'] == 'room')
                                    <div class="item" style="margin-left:20px;float:left;width:45%;">
                                        <i class="checkmark icon"></i>
                                        <div class="content">
                                            <div class="description">
                                                {{ $hotel['avail_facilities']['avail_facilitiy'][$i]['facility_name'] }}
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endfor
                            <div style="clear:both;"></div>
                        </div>
                        <h3 class="ui horizontal divider header">Olah Raga</h3>
                        <div class="ui list" style="width:70%;margin:auto;">
                            @for ($i = 0; $i < count($hotel['avail_facilities']['avail_facilitiy']); $i++)
                                @if ($hotel['avail_facilities']['avail_facilitiy'][$i]['facility_type']=='sport')
                                    <div class="item" style="margin-left:20px;float:left;width:45%;">
                                        <i class="checkmark icon"></i>
                                        <div class="content">
                                            <div class="description">
                                                {{ $hotel['avail_facilities']['avail_facilitiy'][$i]['facility_name'] }}
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endfor
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                    @if (isset($hotel['addinfos']))
                        <div class="ui blue inverted segment">
                            <h3 class="ui header">Informasi Tambahan</h3>
                        </div>
                        <div class="segment">
                            <br>
                            <div class="ui list" style="width:70%;margin:auto;">
                                @for ($i = 0; $i < count($hotel['addinfos']['addinfo']); $i++)
                                    <div class="item" style="margin-left:20px;float:left;width:45%;">
                                        <i class="checkmark icon"></i>
                                        <div class="content">
                                            <div class="description">
                                                {{ $hotel['addinfos']['addinfo'][$i] }}
                                            </div>
                                        </div>
                                    </div>
                                @endfor
                            </div>
                            <div style="clear:both;"></div>
                            <br>
                        </div>
                    @endif
                    @if (isset($hotel['results']))
                        <div class="ui blue inverted segment">
                            <h3 class="ui header">Tipe Kamar</h3>
                        </div>
                        @for ($i = 0; $i < count($hotel['results']['result']); $i++)
                            <div class="ui segment">
                                <h4 class="ui header">{{ $hotel['results']['result'][$i]['room_name'] }}</h4>
                                <div class="ui three column very relaxed stackable grid">
                                    <div class="column">
                                        @if (count($hotel['results']['result'][$i]['all_photo_room'])==1)
                                            <img src="{{ $hotel['results']['result'][$i]['all_photo_room']['0'] }}" class="ui tiny spaced rounded image">
                                        @else
                                            <div class="room photo" id="nanogallery2" data-nanogallery2='{"thumbnailWidth":"auto", "thumbnailHeight":55, "viewerToolbar":{"display":false}, "locationHash": false}'>
                                                @for ($j = 0; $j < count($hotel['results']['result'][$i]['all_photo_room']); $j++)
                                                    <a href="{{ $hotel['results']['result'][$i]['all_photo_room'][$j] }}" data-ngthumb="{{ $hotel['results']['result'][$i]['all_photo_room'][$j] }}" data-ngdesc=""></a>
                                                @endfor
                                            </div>
                                        @endif
                                    </div>
                                    <div class="column" style="text-align:justify;">
                                        @if (array_key_exists('refund_policy', $hotel['results']['result'][$i]))
                                            {{ $hotel['results']['result'][$i]['refund_policy'] }}
                                        @endif
                                    </div>
                                    <div class="column">
                                        <div style="float:right;">
                                            <h4 class="ui header">{{ "Rp " . number_format($hotel['results']['result'][$i]['price'], 0, '.', ',') }}</h4>
                                            @php
                                                $night = $end->diffInDays($start);
                                                $bookuri=str_replace('night=1', 'night='.$night, $hotel['results']['result'][$i]['bookUri']);
                                            @endphp
                                            <a data-url="{{ $hotel['results']['result'][$i]['bookUri'] }}" data-biaya="{{ number_format($hotel['results']['result'][$i]['price'], 0, '', '') }}" data-dana="{{ $dana }}" class="ui yellow pilih button">Pesan</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ui bottom attached footer"></div>
<div class="ui mini modal">
    <div class="header">
        Transaksi Tidak Dapat Dilakukan. Alokasi Dana Perhari Untuk Hotel Adalah Rp. {{ number_format($dana, 0, ',', '.') }}
    </div>
    <div class="actions">
        <div class="ui red cancel button">
            Batal
        </div>
    </div>
</div>
@endsection

@section('script')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChrdDRWwH5tZRc_wDGUGT8gjcTrEIJEns&callback=map"></script>
<script src="https://unpkg.com/nanogallery2@2.2.0/dist/jquery.nanogallery2.min.js"></script>
<script>
    $(function() {
        $('.ui.rating').rating({
            maxRating: 5
        }).rating('disable');
    });

    function map() {
        var latlong={lat:$('#map').data('lat'), lng:$('#map').data('lng')}
        var map=new google.maps.Map(document.getElementById('map'), {
            zoom:15,
            scrollwheel: false,
            center:latlong
        });
        var marker = new google.maps.Marker({
            position: latlong,
            map: map
        });
        $(window).resize(function() {
            google.maps.event.trigger(map, "resize");
        });
    }

    $('.pilih.button').click(function() {
        let url = $(this).data('url');
        let biaya = $(this).data('biaya');
        let dana = $(this).data('dana');
        if (biaya < dana) {
            $('.ui.page.dimmer').dimmer('show');
            axios.post("{{ route('cari.hotel.simpan') }}", {
                b: biaya,
                u: url,
                lat: $('#map').data('lat'),
                lng: $('#map').data('lng')
            }).then((response) => {
                location.replace("{{ route('home.rekap') }}")
                // $('.ui.page.dimmer').dimmer('hide');
            }).catch((error) => {
                $('.ui.page.dimmer').dimmer('hide');

                iziToast.error({
                    title: 'Action Failed',
                    message: error.response.data
                });
            })
        } else {
            $('.ui.mini.modal').modal('show');
        }
    });
</script>
@endsection