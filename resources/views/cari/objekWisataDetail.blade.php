@extends('layouts.home')

@section('style')
<link rel="stylesheet" href="https://unpkg.com/nanogallery2@2.2.0/dist/css/nanogallery2.min.css">
@endsection

@section('content')
<div class="ui container content">
    <div class="top divider"></div>
    <div class="top divider"></div>
    <div class="ui grid">
        <div class="sixteen wide column">
            <div class="ui blue segment">
                @foreach ($objekWisatas as $objekwisata)
                    <h3 class="ui dividing header" data-id="{{ $objekwisata->id }}">
                        {{ $objekwisata->nama }}
                    </h3>
                    <p style="text-align:justify">
                        {!! nl2br($objekwisata->deskripsi) !!}
                    </p>
                    <div class="ui divider"></div>
                    <b>Waktu Operasi:</b> &nbsp{{ $objekwisata->hari_buka }} - {{ $objekwisata->hari_tutup }} &nbsp {{ $objekwisata->jam_buka }} - {{ $objekwisata->jam_tutup }}
                    <br>
                    <b>Alamat:</b> &nbsp{{ $objekwisata->alamat }}
                    <br>
                    <b class="sisa dana" data-sisa-dana="{{ $sisaDana }}">Harga:</b> &nbsp
                        <span class="harga dewasa" data-harga="{{ $objekwisata->harga_dewasa }}" data-jumlah="{{ intval(session('parameter.dewasa')) }}">
                            {{ 'Rp '.number_format($objekwisata->harga_dewasa, 0, '.', ',') }}
                        </span> (Dewasa), &nbsp
                        <span class="harga anak" data-harga="{{ $objekwisata->harga_anak }}" data-jumlah="{{ intval(session('parameter.anak')) }}">
                            {{ 'Rp '.number_format($objekwisata->harga_anak, 0, '.', ',') }}
                        <span> (Anak-Anak)
                    <br>
                    <b>Map:</b>
                    <br>
                    <div class="ui embed" id="map" data-lat="{{ $objekwisata->lat }}" data-lng="{{ $objekwisata->lng }}"></div>
                    <div class="ui hidden divider"></div>
                    <div class="ui divider"></div>
                    <h4 class="ui header">Gambar Objek Wisata</h4>
                    <div class="ui images" id="nanogallery2" data-nanogallery2='{"thumbnailWidth":"auto", "thumbnailHeight":55, "viewerToolbar":{"display":false}, "locationHash": false}'>
                        @foreach ($objekwisata->images as $image)
                            <a href="{{ asset($image->path) }}" data-ngthumb="{{ asset($image->path) }}"></a>
                        @endforeach
                    </div>
                    <div class="ui divider"></div>
                    <a class="ui fluid yellow button" onclick="pilih()">Pilih</a>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="ui bottom attached footer"></div>
@endsection

@section('script')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChrdDRWwH5tZRc_wDGUGT8gjcTrEIJEns&callback=map"></script>
<script src="https://unpkg.com/nanogallery2@2.2.0/dist/jquery.nanogallery2.min.js"></script>
<script>
    function map() {
        let latlong = {
            lat: $('#map').data('lat'),
            lng: $('#map').data('lng')
        }

        let map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            scrollwheel: false,
            center: latlong,
            styles: [{
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [{
                    "visibility": "off"
                }]
            },{
                "featureType": "poi.business",
                "stylers":[{
                    "visibility": "off"
                }]
            },{
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            },{
                "featureType": "transit",
                "stylers": [{
                    "visibility": "off"
                }]
            }]
        });

        let marker = new google.maps.Marker({
            position: latlong,
            map: map
        });
    }
    function pilih() {
        let biaya = ($('.harga.dewasa').data('harga') * $('.harga.dewasa').data('jumlah')) + ($('.harga.anak').data('harga') * $('.harga.anak').data('jumlah'));

        $('.ui.page.dimmer').dimmer('show');
        axios.post("{{ route('cari.objekwisata.simpan') }}", {
            id: $('.ui.dividing.header').data('id'),
            dewasa: $('.harga.dewasa').data('harga') * $('.harga.dewasa').data('jumlah'),
            anak: $('.harga.anak').data('harga') * $('.harga.anak').data('jumlah'),
            latlng: $('#map').data('lat')+','+$('#map').data('lng')
        }).then((response) => {
            location.href = "{{ route('home.rekap') }}"
            // $('.ui.page.dimmer').dimmer('hide');
        }).catch((error) => {
            $('.ui.page.dimmer').dimmer('hide');

            iziToast.error({
                title: 'Action Failed',
                message: error.response.data
            });
        })
    }
</script>
@endsection