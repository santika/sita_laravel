@extends('layouts.home')

@section('content')
<header>
    <div class="masthead">
        <div class="top divider home"></div>
        <div class="ui container">
            <div class="ui segments">
                <div class="ui segment">
                    <div class="ui divided items">
                        <div class="ui item">
                            <div class="content" style="margin:unset !important;">
                                <div class="header">
                                    Dana Awal:
                                </div>
                                <div style="float:right;font-size:1.28571429em;font-weight:700;">
                                    <a class="ui yellow button" href="{{ route('landingpage.index') }}">Ubah</a>
                                    Rp. {{ $parameters['dana'] }}
                                </div>
                            </div>
                        </div>
                        <div class="ui item">
                            <div class="content" style="margin:unset !important;">
                                <div class="header">
                                    Total Biaya Keseluruhan:
                                </div>
                                <div style="float:right;font-size:1.28571429em;font-weight:700;">
                                    Rp. {{ number_format($totalBiaya, 0, ',', '.') }}
                                </div>
                            </div>
                        </div>
                        <div class="ui item">
                            <div class="content" style="margin:unset !important;">
                                <div class="header">
                                    Sisa Dana:
                                </div>
                                <div style="float:right;font-size:1.28571429em;font-weight:700;">
                                    Rp. {{ number_format($sisaDana, 0, ',', '.') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ui blue inverted clearing segment">
                    <h3 style="float:left;">Data Penerbagan</h3>
                    @if (is_null(session('dataWisatawan.0')))
                        <a class="ui right floated yellow button pesawat" href="{{ route('home.tambahwisatawan') }}" data-order-pesawat="null">Pilih</a>
                    @else
                        <a class="ui right floated yellow button pesawat" href="{{ route('cari.pesawat') }}" data-order-pesawat="not null">Ubah</a>
                    @endif
                </div>
                <div class="ui segment">
                    <div class="ui divided items">
                        @if (is_null(session('order.pesawat')))
                            <div class="item">
                                <div class="content">
                                    <div class="ui visible negative message">
                                        <div class="header">
                                            Penerbangan Belum Dipilih
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            @php $pesawat = session('order.pesawat'); @endphp
                            @for ($i = 0; $i < count($pesawat['myorder']['data']); $i++)
                                @if ($pesawat['myorder']['data'][$i]['order_type'] == 'flight')
                                    <div class="ui item">
                                        <div class="ui tiny image">
                                            <img src="{{ $pesawat['myorder']['data'][$i]['order_photo'] }}" alt="{{ $pesawat['myorder']['data'][$i]['detail']['airlines_name'].' Logo' }}">
                                        </div>
                                        <div class="content">
                                            <div class="header">
                                                {{ $pesawat['myorder']['data'][$i]['order_name_detail'] }}, {{ $pesawat['myorder']['data'][$i]['detail']['departure_time'] }} - {{ $pesawat['myorder']['data'][$i]['detail']['arrival_time'] }}
                                            </div>
                                            <div class="meta">
                                                <span class="departure">{{ $pesawat['myorder']['data'][$i]['order_name'] }}</span>
                                                <span class="date">{{ $pesawat['myorder']['data'][$i]['detail']['flight_date'] }}</span>
                                            </div>
                                            <div class="description">
                                                <div style="float:right;">
                                                    @for ($total = 0; $total < count($pesawat['myorder']['data'][$i]['detail']['breakdown_price']); $total++)
                                                        @if ($pesawat['myorder']['data'][$i]['detail']['breakdown_price'][$total]['category'] == 'total base price')
                                                            <b>Total Harga Tiket: Rp. {{ number_format($pesawat['myorder']['data'][$i]['detail']['breakdown_price'][$total]['value'], 0, ',', '.') }}</b><br>
                                                        @endif
                                                    @endfor
                                                    <b>Pajak: Rp. {{ number_format($pesawat['myorder']['data'][$i]['tax_and_charge'], 0, ',', '.') }}</b><br>
                                                    <b>Total Biaya: Rp. {{ number_format($pesawat['myorder']['data'][$i]['subtotal_and_charge'], 0, ',', '.') }}</b><br>
                                                </div>
                                                @for ($dewasa = 0; $dewasa < count($pesawat['myorder']['data'][$i]['detail']['passengers']['adult']); $dewasa++)
                                                    <h5>Dewasa: Rp. {{ number_format($pesawat['myorder']['data'][$i]['detail']['breakdown_price'][0]['value'], 0, '.', ',') }}</h5>
                                                    {{ $pesawat['myorder']['data'][$i]['detail']['passengers']['adult'][$dewasa]['title'].' '.$pesawat['myorder']['data'][$i]['detail']['passengers']['adult'][$dewasa]['first_name'].' '.$pesawat['myorder']['data'][$i]['detail']['passengers']['adult'][$dewasa]['last_name'].' - '.$pesawat['myorder']['data'][$i]['detail']['passengers']['adult'][$dewasa]['birth_date'] }}
                                                    @if (!empty($pesawat['myorder']['data'][$i]['detail']['passengers']['adult'][$dewasa]['check_in_baggage']))
                                                        , Bagasi {{ $pesawat['myorder']['data'][$i]['detail']['passengers']['adult'][$dewasa]['check_in_baggage_size'].' '.$pesawat['myorder']['data'][$i]['detail']['passengers']['adult'][$dewasa]['check_in_baggage_unit'] }}
                                                    @endif
                                                @endfor
                                                @if (isset($pesawat['myorder']['data'][$i]['detail']['passengers']['child']))
                                                    @for ($anak = 0; $anak < count($pesawat['myorder']['data'][$i]['detail']['passengers']['child']); $anak++)
                                                        @for ($breakdown = 0; $breakdown < count($pesawat['myorder']['data'][$i]['detail']['breakdown_price']); $breakdown++)
                                                            @if ($pesawat['myorder']['data'][$i]['detail']['breakdown_price'][$breakdown]['category'] == 'price child')
                                                                <h5>Anak-Anak: Rp. <?php // echo number_format($pesawat['myorder']['data'][$i]['detail']['breakdown_price'][$breakdown]['value'],0,'.',',') ?></h5>
                                                            @endif
                                                        @endfor
                                                        {{ $pesawat['myorder']['data'][$i]['detail']['passengers']['child'][$anak]['title'].' '.$pesawat['myorder']['data'][$i]['detail']['passengers']['child'][$anak]['first_name'].' '.$pesawat['myorder']['data'][$i]['detail']['passengers']['child'][$anak]['last_name'].' - '.$pesawat['myorder']['data'][$i]['detail']['passengers']['child'][$anak]['birth_date'] }}
                                                        @if (!empty($pesawat['myorder']['data'][$i]['detail']['passengers']['child'][$anak]['check_in_baggage']))
                                                            , Bagasi <?php // echo $pesawat['myorder']['data'][$i]['detail']['passengers']['child'][$anak]['check_in_baggage_size'].' '.$pesawat['myorder']['data'][$i]['detail']['passengers']['child'][$anak]['check_in_baggage_unit'] ?>
                                                        @endif
                                                    @endfor
                                                @endif
                                                @if (isset($pesawat['myorder']['data'][$i]['detail']['passengers']['infant']))
                                                    @for ($batita = 0; $batita < count($pesawat['myorder']['data'][$i]['detail']['passengers']['infant']); $batita++)
                                                        @for ($breakdown = 0; $breakdown < count($pesawat['myorder']['data'][$i]['detail']['breakdown_price']); $breakdown++)
                                                            @if ($pesawat['myorder']['data'][$i]['detail']['breakdown_price'][$breakdown]['category']=='price infant')
                                                                <h5>Batita: Rp. {{ number_format($pesawat['myorder']['data'][$i]['detail']['breakdown_price'][$breakdown]['value'], 0, ',', '.') }}</h5>
                                                            @endif
                                                        @endfor
                                                        {{ $pesawat['myorder']['data'][$i]['detail']['passengers']['infant'][$batita]['title'].' '.$pesawat['myorder']['data'][$i]['detail']['passengers']['infant'][$batita]['first_name'].' '.$pesawat['myorder']['data'][$i]['detail']['passengers']['infant'][$batita]['last_name'].' - '.$pesawat['myorder']['data'][$i]['detail']['passengers']['infant'][$batita]['birth_date'] }}
                                                        @if (!empty($pesawat['myorder']['data'][$i]['detail']['passengers']['infant'][$batita]['check_in_baggage']))
                                                            , Bagasi {{ $pesawat['myorder']['data'][$i]['detail']['passengers']['infant'][$batita]['check_in_baggage_size'].' '.$pesawat['myorder']['data'][$i]['detail']['passengers']['infant'][$batita]['check_in_baggage_unit'] }}
                                                        @endif
                                                    @endfor
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endfor
                            <div class="ui item">
                                <div class="content" style="margin:unset !important;">
                                    <div class="header">
                                        Total Biaya Tiket Pesawat:
                                    </div>
                                    <div style="float:right;font-size:1.28571429em;font-weight:700;">
                                        Rp. {{ number_format($pesawat['myorder']['data'][0]['subtotal_and_charge']+$pesawat['myorder']['data'][1]['subtotal_and_charge'], 0, ',', '.') }}
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="ui blue inverted clearing segment">
                    <h3 style="float:left;">Data Hotel Terpilih</h3>
                    @if (is_null(session('order.hotel')))
                        <a class="ui right floated yellow button hotel" href="{{ route('cari.hotel') }}" data-order-hotel="null">Pilih</a>
                    @else
                        <a class="ui right floated yellow button hotel" href="{{ route('cari.hotel') }}" data-order-hotel="not null">Ubah</a>
                    @endif
                </div>
                <div class="ui segment">
                    @if (is_null(session('order.hotel')))
                        <div class="item">
                            <div class="content">
                                <div class="ui visible negative message">
                                    <div class="header">
                                        Hotel Belum Dipilih
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        @php $hotel = session('order.hotel') @endphp
                        @for ($i = 0; $i < count($hotel['myorder']['data']); $i++)
                            @if ($hotel['myorder']['data'][$i]['order_type'] == 'hotel')
                                <div class="ui items">
                                    <div class="ui item">
                                        <div class="ui tiny image">
                                            <img src="{{ $hotel['myorder']['data'][$i]['order_photo'] }}" alt="{{ $hotel['myorder']['data'][$i]['order_name'].' Logo' }}">
                                        </div>
                                        <div class="content">
                                            <div style="float:right;">
                                                <b>Harga Per Malam: Rp. {{ number_format($hotel['myorder']['data'][$i]['detail']['price_per_night'], 0, '.' ,',') }}</b><br>
                                                <b>Total Biaya: Rp. {{ number_format($hotel['myorder']['data'][$i]['subtotal_and_charge'], 0, '.', ',') }}</b>
                                            </div>
                                            <div class="header">
                                                {{ $hotel['myorder']['data'][$i]['order_name'] }} - {{ $hotel['myorder']['data'][$i]['order_name_detail'] }}
                                            </div>
                                            <div class="meta">
                                                <span class="date">{{ $hotel['myorder']['data'][$i]['detail']['startdate'] }} - {{ $hotel['myorder']['data'][$i]['detail']['enddate'] }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endfor
                    @endif
                </div>
                <div class="ui blue inverted clearing segment">
                    <h3 style="float:left;">Data Objek Wisata Terpilih</h3>
                    @if (is_null(session('objekWisata')))
                        <a class="ui right floated yellow button" href="{{ route('cari.objekwisata') }}">Pilih</a>
                    @elseif ($jumlahObjekWisata > count(session('objekWisata')))
                        <a class="ui right floated yellow button" href="{{ route('cari.objekwisata') }}">Tambah</a>
                    @endif
                </div>
                <div class="ui segment">
                    @if (is_null(session('objekWisata')))
                        <div class="item">
                            <div class="content">
                                <div class="ui visible warning message">
                                    <div class="header">
                                        Objekwisata Belum Dipilih
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="ui divided items">
                            @php $totalBiaya = 0; @endphp
                            @foreach ($objekWisatas as $objekwisata)
                                <div class="ui item">
                                    <div class="ui small image">
                                        @php $images = $objekwisata->images @endphp
                                        @foreach ($images as $image)
                                            @if ($loop->first)
                                                <img src="{{ asset($image->path) }}" alt="{{ $objekwisata->nama }}">
                                                @break
                                            @endif
                                        @endforeach
                                    </div>
                                    <div class="content">
                                        <div style="float:right;width:20%;">
                                            <b>Total Biaya: Rp. {{ number_format(session('biaya.objekWisata.'.$loop->index), 0, '.', ',') }}</b>
                                            <br>
                                            <a class="ui fluid yellow button" href="{{ route('cari.objekwisata', [$objekwisata->id]) }}">Ubah</a>
                                        </div>
                                        <div class="header">
                                            {{ $objekwisata->nama }}
                                        </div>
                                        <div class="meta">
                                            , <span class="price">Harga Dewasa Rp. {{ number_format($objekwisata->harga_dewasa, 0, '.', ',') }}, Harga Anak-Anak Rp. {{ number_format($objekwisata->harga_anak, 0, '.', ',') }}</span>
                                        </div>
                                    </div>
                                </div>
                                @php $totalBiaya = $totalBiaya + intval(session('biaya.objekWisata.'.$loop->index)) @endphp
                            @endforeach
                            <div class="ui item">
                                <div class="content" style="margin:unset !important;">
                                    <div class="header">
                                        Total Biaya Objek Wisata:
                                    </div>
                                    <div style="float:right;font-size:1.28571429em;font-weight:700;">
                                        Rp. {{ number_format($totalBiaya,0, '.', ',') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="ui blue inverted clearing segment">
                    <h3 style="float:left;">Data Mobil Terpilih</h3>
                    @if (is_null(session('mobil')))
                        <a class="ui right floated yellow button" href="{{ route('cari.mobil') }}">Pilih</a>
                    @else
                        <a class="ui right floated yellow button" href="{{ route('cari.mobil') }}">Ubah</a>
                    @endif
                </div>
                <div class="ui segment">
                    @if (is_null(session('mobil')))
                        <div class="item">
                            <div class="content">
                                <div class="ui visible warning message">
                                    <div class="header">
                                        Mobil Belum Dipilih
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="ui items">
                            <div class="ui item">
                                @foreach ($mobils as $mobils)
                                    <div class="ui small image">
                                        <img src="{{ asset($mobils->image_path) }}" alt="{{ $mobils->model }}">
                                    </div>
                                    <div class="content">
                                        <div style="float:right;width:20%;">
                                            <b>Biaya: Rp. {{ number_format($mobil, 0, '.', ',') }}</b>
                                        </div>
                                        <div class="header">
                                            {{ $mobils->model }}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
                <div class="ui segment">
                    <div class="ui fluid yellow lanjutkan button" href="{{ route('cari.simpan') }}">
                        Lanjutkan
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ui bottom attached footer"></div>
</header>
@endsection

@section('script')
<script>
    if ($('.hotel').data('orderHotel') == null || $('.pesawat').data('orderPesawat') == null) {
        $('.ui.fluid.yellow.lanjutkan.button').addClass('disabled');
    }

    $('.ui.fluid.yellow.lanjutkan.button').click(function () {
        $('.page.dimmer').dimmer('show');
        axios.post("{{ route('cari.simpan') }}")
            .then((response) => {
                axios.post("{{ route('jadwal.generate') }}/", {
                    id:response.data
                })
                    .then((response) => {
                        location.href = "{{ route('jadwal') }}/" + response.data;
                    })
                    .catch((error) => {
                        $('.page.dimmer').dimmer('hide');
                        iziToast.error({
                            title: 'Action Failed',
                            message: error.response.data
                        });
                    });
                // $('.page.dimmer').dimmer('hide');
            })
            .catch((error) => {
                $('.page.dimmer').dimmer('hide');
                iziToast.error({
                    title: 'Action Failed',
                    message: error.response.data
                });
            });
    })
</script>
@endsection