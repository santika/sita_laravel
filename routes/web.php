<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('logout', function () {
    Auth::logout();
    Session::flush();
    return redirect()->route('landingpage.index');
})->name('logout');
Route::get('login', function () {
    return redirect()->route('landingpage.index');
})->name('login');

Route::get('/', 'HomeController@index')->name('landingpage.index');

Route::middleware(['auth'])->group(function () {
    Route::middleware(['user'])->group(function () {
        Route::post('simpan', 'HomeController@simpanParameter')->name('landingpage.simpanparameter');
        Route::get('tambahwisatawan', 'HomeController@tambahWisatawan')->name('home.tambahwisatawan');
        Route::post('tambahwisatawan', 'HomeController@tambahWisatawanSimpan')->name('home.tambahwisatawan');
        Route::get('rekap', 'HomeController@rekap')->name('home.rekap');

        Route::prefix('cari')->group(function () {
            Route::prefix('pesawat')->group(function () {
                Route::get('/', 'CariController@pesawat')->name('cari.pesawat');
                Route::get('pergi', 'CariController@pesawatAwal')->name('cari.pesawat.pergi');
                Route::get('pulang', 'CariController@pesawatKembali')->name('cari.pesawat.pulang');
                Route::post('simpan', 'HomeController@simpanPesawat')->name('cari.pesawat.simpan');
            });
            Route::prefix('hotel')->group(function () {
                Route::get('/', 'CariController@hotel')->name('cari.hotel');
                Route::post('simpan', 'HomeController@simpanHotel')->name('cari.hotel.simpan');
                Route::post('{detail?}', 'CariController@getHotel')->name('cari.hotel.get');
                Route::get('detail', 'CariController@detailHotel')->name('cari.hotel.detail');
            });
            Route::prefix('objekwisata')->group(function () {
                Route::get('/{id?}', 'CariController@objekWisata')->where('id', '[0-9]+')->name('cari.objekwisata');
                Route::get('detail/{id}', 'CariController@detailObjekWisata')->name('cari.objekwisata.detail');
                Route::post('simpan', 'HomeController@simpanObjekWisata')->name('cari.objekwisata.simpan');
                Route::get('delete', 'HomeController@deleteObjekWisata');
            });
            Route::prefix('mobil')->group(function () {
                Route::get('/', 'CariController@mobil')->name('cari.mobil');
                Route::post('simpan', 'HomeController@simpanMobil')->name('cari.mobil.simpan');
            });
            Route::post('simpan', 'CariController@simpan')->name('cari.simpan');
            Route::prefix('jadwal')->group(function () {
                Route::get('monitor/{id?}', 'JadwalController@monitor');
                Route::get('/{id?}', 'JadwalController@index')->name('jadwal');
                Route::get('download/{id?}', 'JadwalController@download')->name('jadwal.download');
                Route::post('generate', 'JadwalController@generate')->name('jadwal.generate');
            });
        });
    });

    Route::prefix('otomatis')->group(function () {
        Route::post('/', 'OtomatisController@index')->name('otomatis');
        Route::get('/', 'OtomatisController@index');
        Route::get('clear', 'OtomatisController@clear');
    });

    Route::prefix('admin')->middleware(['admin'])->group(function () {
        Route::get('/', 'AdminController@index')->name('admin.home');

        Route::prefix('objekwisata')->group(function () {
            Route::get('/', 'AdminController@objekWisata')->name('admin.objekwisata');
            Route::get('tambah', 'AdminController@tambahObjekWisata')->name('admin.objekwisata.tambah');
            Route::post('tambah', 'AdminController@storeObjekWisata')->name('admin.objekwisata.store');
            Route::get('detail/{id}', 'AdminController@detailObjekWisata')->where('id', '[0-9]+')->name('admin.objekwisata.detail');
        });
        Route::prefix('mobil')->group(function () {
            Route::get('/', 'AdminController@mobil')->name('admin.mobil');
            Route::get('tambah', 'AdminController@tambahMobil')->name('admin.mobil.tambah');
            Route::post('tambah', 'AdminController@storeMobil')->name('admin.mobil.store');
        });
    });
});